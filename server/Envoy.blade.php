@servers(['web' => ['crfweb@bianca']])

@setup
$repository = 'git@gitlab.com:chiny1/liveeventsttv.git';
$releases_dir = '/var/www/virtual/crfweb/backend/liveeventsttv/releases';
$app_dir = '/var/www/virtual/crfweb/backend/liveeventsttv/server';
$pub_dir = '/var/www/virtual/crfweb/html/lens';
$release = date('d-m-Y_H-i-s');
$new_release_dir = $releases_dir .'/'. $release;
$new_release_dir_server = $releases_dir .'/'. $release.'/server';
$current_dir = '/var/www/virtual/crfweb/backend/liveeventsttv/current/server';
$commit = 'origin/master';
@endsetup

@story('deploy')
clone_repository
run_composer
update_symlinks
run_migrations
run_npm
run_websockets
@endstory

@task('run_websockets')
echo 'Starting websockets server...'
cd {{ $new_release_dir_server }}
php artisan websockets:serve
@endtask

@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git checkout master
git pull origin master
@endtask

@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir_server }}
echo "install compose dependencies..."
composer install --prefer-dist --no-scripts -q -o
php artisan optimize:clear
composer dump-autoload
@endtask

@task('run_npm')
cd {{ $new_release_dir_server }}
echo "Install node modules"
npm install
echo "Run production build..."
npm run prod
@endtask

@task('update_symlinks')
echo "Linking storage directory"
rm -rf {{ $new_release_dir_server }}/storage
rm -rf {{ $new_release_dir_server }}/public/images
rm -rf {{ $new_release_dir_server }}/public/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir_server }}/storage
echo "Linking .env file"
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir_server }}/.env
echo "Linking current release"
ln -nfs {{ $new_release_dir_server }} {{ $current_dir }}
echo "Linking public folder"
ln -nfs {{ $current_dir }}/public {{ $public_dir }}
cd {{ $new_release_dir_server }}
php artisan storage:link
@endtask

@task('update_links')
echo "try updating links for current..."
echo 'Linking public folder'
ln -nfs {{ $current_dir }}/public {{ $public_dir }}
@endtask

@task('run_migrations')
echo "Updating migrations..."
cd {{ $new_release_dir_server }}
php artisan migrate --force
@endtask