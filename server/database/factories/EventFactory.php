<?php

namespace Database\Factories;

use App\Enums\VisibilityEnum;
use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $startDate = $this->faker->date('d.m.Y');
        return [
            'title' => $this->faker->text(60),
            'start_date' => $startDate,
            'end_date' => $this->faker->date('d.m.Y'),
            'event_description' => $this->faker->realText(500),
            'event_mode' => $this->faker->numberBetween(1,3),
            'vis_id' => 1,
        ];
    }
}
