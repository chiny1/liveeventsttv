<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_details', function (Blueprint $table) {
            $table->id();

            $table->string('field_value');
            $table->unsignedBigInteger('score')->string(0);

            $table->unsignedBigInteger('point_field_id');
            $table->foreign('point_field_id')->references('id')->on('point_fields');

            $table->unsignedBigInteger('match_id');
            $table->foreign('match_id')->references('id')->on('match_data');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_details');
    }
}
