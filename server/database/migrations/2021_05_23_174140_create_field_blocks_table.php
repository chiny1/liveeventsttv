<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_blocks', function (Blueprint $table) {
            $table->id();

            $table->string('component_name');
            $table->json('data');

            $table->unsignedBigInteger('point_field_id');
            $table->foreign('point_field_id')->references('id')->on('point_fields');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_blocks');
    }
}
