<?php

use App\Enums\VisibilityEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_modes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('mode_description')->nullable();
            $table->timestamps();
        });

        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('event_description')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            
            $table->unsignedBigInteger('event_mode');
            $table->foreign('event_mode')->references('id')->on('event_modes');

            $table->unsignedBigInteger('vis_id');
            $table->foreign('vis_id')->references('id')->on('event_visibilities');

            $table->unsignedBigInteger('owner')->nullable();
            $table->foreign('owner')->references('id')->on('users');

            $table->unsignedBigInteger('point_template_id')->nullable();
            $table->foreign('point_template_id')->references('id')->on('point_templates');


            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
        Schema::dropIfExists('event_modes');
    }
}
