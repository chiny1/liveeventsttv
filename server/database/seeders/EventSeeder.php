<?php

namespace Database\Seeders;

use App\Enums\EventModeEnum;
use App\Enums\VisibilityEnum;
use App\Models\Event;
use App\Models\EventMode;
use App\Models\EventOrganizer;
use App\Models\EventUser;
use App\Utils\EnvironmentHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //base data
        //Event Modes
        $onlySolos = new EventMode();
        $onlySolos->name = EventModeEnum::SOLO;
        $onlySolos->mode_description = "Only solo players can participate in this event";
        $onlySolos->save();

        $onlyTeams = new EventMode();
        $onlyTeams->name = EventModeEnum::TEAM;
        $onlyTeams->mode_description = "Only teams can participate in this event";
        $onlyTeams->save();

        //Add only Follower
        //$onlyFollower->
        $both = new EventMode();
        $both->name = EventModeEnum::BOTH;
        $both->mode_description = "Both solo players and teams can participate in this event";
        $both->save();

        if(EnvironmentHelper::isDevelopement()) {
            $this->command->info("Setup test events");
            $this->setupTestEvents();
            //make fake data
            Event::factory()->count(100)->create();
        } else {
            $this->command->info("Production event setup..");
        }
    }

    public function setupTestEvents() {
        $event1 = $this->addEvent("Hunt Showdown: ChinyONE[TTV] Part I", "Starte mit uns in unser erstes Live event. ....", 1, 1, 1);
        $this->addUserAsOrganizer(2, $event1->id);
        $this->addUserAsOrganizer(3, $event1->id);
        $this->addUserAsOrganizer(4, $event1->id);

        $event2 = $this->addEvent("Hunt Showdown: ChinyONE[TTV] Part II", "Starte mit uns in unser erstes Live event. ....", 2, 2, 1);
        $this->addUserAsOrganizer(2, $event2->id);
        $this->addUserAsOrganizer(3, $event2->id);

        $event3 = $this->addEvent("Hunt Showdown: ChinyONE[TTV] Part III letztes Event", "Starte mit uns in unser erstes Live event. ....", 3, 2, 1);
        $this->addUserAsOrganizer(2, $event3->id);
    }

    private function addEvent($title, $descr, $mode = 3, $visibility = 1, $owner) {
        $event = new Event();
        $event->title = $title;
        $event->start_date = Carbon::now(20);
        $event->end_date = Carbon::now()->addDays(30);
        $event->event_description = $descr;
        $event->event_mode = $mode; //
        $event->vis_id = $visibility;
        $event->owner = $owner;
        $event->save();

        return $event;
    }

    private function addUserToEvent($userId, $eventId) {
        $participate = new EventUser();
        $participate->user_id = $userId;
        $participate->event_id = $eventId;
        $participate->save();

        return $participate;
    }

    private function addUserAsOrganizer($userId, $eventId) {
        $organize = new EventOrganizer();
        $organize->user_id = $userId;
        $organize->event_id = $eventId;
        $organize->save();

        return $organize;
    }
}
