<?php

namespace Database\Seeders;

use App\Enums\PermissionsEnum;
use App\Enums\RolesEnum;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create roles
        $adminRole = Role::create(['guard_name'=>'api', 'name' =>RolesEnum::ADMIN]);
        $userRole = Role::create(['guard_name'=>'api', 'name' =>RolesEnum::USER]);
        $organizer = Role::create(['guard_name' => 'api', 'name' => RolesEnum::ORGANIZER]);
        $reviewer = Role::create(['guard_name' => 'api', 'name' => RolesEnum::REVIEWER]);
        
        //admin permissions
        $adminRole->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::USER_CREATE]));
        $adminRole->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::USER_UPDATE]));
        $adminRole->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::USER_DELETE]));
        $adminRole->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::USER_SHOW]));

        //organizer permissions
        $organizer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_CREATE]));
        $organizer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_UPDATE]));
        $organizer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_DELETE]));
        $organizer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_SHOW]));

        //reviewer permissions
        $reviewer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::DISQUALIFY_TEAM]));
        $reviewer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::DISQUALIFY_USER]));
        $reviewer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_SUBMISSION_ACCEPT]));
        $reviewer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_SUBMISSION_DECLINE]));
        $reviewer->givePermissionTo(Permission::create(['guard_name'=>'api', 'name' => PermissionsEnum::EVENT_SUBMISSION_REVIEW]));

        //user permissions
        $userRole->givePermissionTo(PermissionsEnum::USER_SHOW);
        $userRole->givePermissionTo(PermissionsEnum::EVENT_SHOW);
        $userRole->givePermissionTo(PermissionsEnum::EVENT_UPDATE);
        $userRole->givePermissionTo(PermissionsEnum::EVENT_CREATE);
        $userRole->givePermissionTo(PermissionsEnum::EVENT_DELETE);

        //sync permissions
    }
}
