<?php

namespace Database\Seeders;

use App\Enums\VisibilityEnum;
use App\Models\EventVisibility;
use Illuminate\Database\Seeder;

class VisibilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $v = new EventVisibility();
        $v->visibility = VisibilityEnum::PUBLIC;
        $v->save();

        $v = new EventVisibility();
        $v->visibility = VisibilityEnum::PRIVATE;
        $v->save();
    }
}
