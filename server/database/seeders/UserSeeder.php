<?php

namespace Database\Seeders;

use App\Enums\RolesEnum;
use App\Models\User;
use App\Utils\EnvironmentHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Enums\PermissionsEnum;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addUsers();
    }

    /**
     * Base Users
     *
     * @return void
     */
    public function addUsers() {
         //base user
         $user = new User();
         $user->name = "ChinyONE";
         $user->email = "chiny1@web.de";
         $user->email_verified_at = Carbon::now()->toDateTime();
         $user->password = Hash::make("1234");
         $user->twitch_channel_name = "chinyone";
         $user->bio = "Hey, it´s me :)";
         $user->save();
         
         $user->assignRole(RolesEnum::ADMIN);
 
         if (EnvironmentHelper::isDevelopement()) {
             
             $this->addUser("KingOfPfand", "king@web.de", "kingofpfand");
             $this->addUser("Tri2Realize", "tri2.realize@gmail.com", "Tri2Realize");
             $this->addUser("Swyneth", "Swyneth@gmail.com", "Swyneth");
             $this->addUser("KetriZS", "ketriz@gmail.com", "ketrizzs");
             $this->addUser("Trymacs", "trymacs@gmail.com", "trymacs");
             $this->addUser("Jen Nyan", "Jen.nyan@gmail.com", "JenNyan");
             $this->addUser("PsychoGhost", "psycho.ghost@gmail.com", "psychoghost");
             $this->addUser("Nufnhe", "nufnhe@gmail.com", "nufnhe");
             
             //create 100 TestUser
             User::factory()->count(100)->create();
         }
    }

    public function addUser($name, $email, $channel, $bio = "") {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->twitch_channel_name = $channel;
        $user->bio = $bio;
        $user->email_verified_at = Carbon::now()->toDateTime();
        $user->password = Hash::make("1234");
        $user->save();

        $user->assignRole(RolesEnum::USER);
    }

}
