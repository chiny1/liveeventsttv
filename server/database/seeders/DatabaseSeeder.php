<?php

namespace Database\Seeders;

use App\Utils\EnvironmentHelper;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info("Seeding for: " .EnvironmentHelper::getEnvironmentMode());

        $this->call([
            RolesAndPermissionsSeeder::class,
            UserSeeder::class,
            VisibilitySeeder::class,
            EventSeeder::class,
        ]);
    }
}
