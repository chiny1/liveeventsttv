@extends('layouts.auth')

@section('content')

@if(session('accessToken'))
<input type="hidden" name="token" id="token" value="{{session('accessToken')}}" />
<input type="hidden" id="redirectPath" value="{{ route('home') }}"/>

<login-component token="{{session('accessToken')}}"></login-component>

@else
<div class="login-page bg-dark">
    <h2>Starting App: NO-API Mode ... </h2>
    <h4>If you see this, pls contact support!</h4>
</div>
@push('footer-scripts')
    <script>
        window.location.replace("{{route('home')}}");
    </script>
@endpush
@endif
@endsection
