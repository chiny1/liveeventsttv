<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <script href="{{asset('plugins/fontawesome-free/js/all.min.js')}}"></script>

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @stack('header-scripts')
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed layout-fixed">
    <div id="app" v-bind:class="{ 'dark-mode': darkMode }">
        <div class="wrapper">
            @include('navigation.main')


            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                    <img src="dist/img/logo.png" alt="Event System Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-flat" data-widget="treeview" role="menu" data-accordion="true">
                            <li class="nav-item">
                                @if(Auth::check())
                                <!-- Sidebar user panel (optional) -->
                                <div class="nav-link">
                                    <div class="mt-5 d-flex">
                                        <div class="image p-1">
                                            @php
                                                $logo = !empty(Auth::user()->logo) ? Auth::user()->logo : asset('dist/img/defaultAvatar.png');
                                            @endphp
                                            <img src="{{$logo}}" class="img-circle elevation-2" style="max-width: 34px" alt="User Image">
                                        </div>
                                        <div style="display: flex; align-items: center; justify-content: center">
                                            <router-link class="pl-3" style="display: content" :to="{name: 'profile'}">{{ Auth::user()->name }}</router-link>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </li>
                            <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <router-link :to="{name: 'events'}" class="nav-link">
                                    <i class="fas fa-home nav-icon"></i>
                                    <p>Home</p>
                                </router-link>
                            </li>

                            <li class="nav-item">
                                <router-link :to="{name: 'my-events'}" class="nav-link">
                                    <i class="fas fa-calendar nav-icon"></i>
                                    <p>Events</p>
                                </router-link>
                            </li>
                            
                            <!--<li class="nav-item">
                                <router-link :to="{name: 'template-add'}" class="nav-link">
                                    <i class="fas fa-shapes nav-icon"></i>
                                    <p>Templates</p>
                                </router-link>
                            </li>-->

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-gamepad"></i>
                                    <p>
                                        Matches
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <router-link :to="{name: 'submissions-history'}" class="nav-link">
                                            <i class="fas fa-clock nav-icon"></i>
                                            <p>History</p>
                                        </router-link>
                                    </li>
                                    <li class="nav-item">
                                         <router-link :to="{name: 'submissions'}" class="nav-link" exact>
                                            <i class="fas fa-table nav-icon"></i>
                                            <p>Submit</p>
                                        </router-link>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <router-link :to="{name: 'my-team'}" class="nav-link">
                                    <i class="fas fa-users nav-icon"></i>
                                    <p>Teams</p>
                                </router-link>
                            </li>

                             <li class="nav-item">
                                <router-link :to="{name: 'myChannel'}" class="nav-link">
                                    <i class="fab fa-twitch nav-icon"></i>
                                    <p>Channel</p>
                                </router-link>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Settings
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <router-link :to="{name: 'profile'}" class="nav-link">
                                            <i class="fas fa-user-circle nav-icon"></i>
                                            <p>Profile</p>
                                        </router-link>
                                    </li>
                                    <li class="nav-item">
                                        <router-link :to="{name: 'my-liveticker'}" class="nav-link">
                                            <i class="fas fa-rss nav-icon"></i>
                                            <p>Live Ticker</p>
                                        </router-link>
                                    </li>
                                </ul>
                            </li>
                            <!-- Only Visible for Admins -->
                            <li class="nav-item" v-if='isAdmin'>
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Administration
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <router-link to="/user" class="nav-link">
                                            <i class="fas fa-users-cog nav-icon"></i>
                                            <p>User Manager</p>
                                        </router-link>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" role="button">
                                    <i class="fas fa-sign-out-alt nav-icon"></i>
                                    <p> {{ __('Logout') }} </p>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->

                </div>
                <!-- /.sidebar -->
            </aside>


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- result list search -->
                <event-search-response-list-component @on-search-close="clearSearchTerm()" :searchTerm="searchTerm" :results="searchResult" :isSearching="isSearching"></event-search-response-list-component>
                <!-- Main content -->
                <div class="content p-0">
                    <div class="container-fluid">
                        <!--content -->
                        @yield('content')
                        <br>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
                <div class="p-3">
                    <h5>Title</h5>
                    <p>Sidebar content</p>
                </div>
            </aside>
            <!-- /.control-sidebar -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">
                    Webapp v{{env('APP_VERSION')}}
                </div>
                <!-- Default to the left -->
                <strong>Copyright &copy; 2021 <a href="https://adminlte.io">LiveTicker TTV</a>.</strong>
            </footer>
        </div>
    </div>

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>

    <!--custom scripts-->
    @stack('footer-scripts')
</body>


</html>

