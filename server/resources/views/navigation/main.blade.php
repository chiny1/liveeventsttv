 <!-- Navbar Main -->
 <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
     <!-- Left navbar links -->
     <ul class="navbar-nav">
         <li class="nav-item">
             <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
         </li>
         <!--  -->
         <li class="nav-item d-none d-sm-inline-block">
             <router-link :to="{name: 'events'}" class="nav-link"> Home </router-link>
         </li>
     </ul>

     <!-- Right navbar links -->
     <ul class="navbar-nav ml-auto">
         <!-- search -->
         <li class="nav-item">
             <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                 <i class="fas fa-search"></i>
             </a>
             <div class="navbar-search-block">
                 <form class="form-inline">
                     <div class="input-group input-group-md w-100">
                         <input v-model="searchTerm" class="form-control form-control-navbar" type="search" placeholder="Search Events..." aria-label="Search">
                         <div class="input-group-append">
                             <button class="btn btn-navbar" @click="search()" type="submit">
                                 <i class="fas fa-search"></i>
                             </button>
                             <button id="cancelSearch" class="btn btn-navbar" @click="clearSearchTerm()" type="button" data-widget="navbar-search">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                 </form>
             </div>
         </li>
         <!-- Darkmode -->
         <li class="nav-item">
             <div class="nav-link">
                 <div class="custom-control custom-switch">
                     <input type="checkbox" class="custom-control-input" id="darkModeSwitch" v-model="darkMode">
                     <label class="custom-control-label" for="darkModeSwitch">Darkmode</label>
                 </div>
             </div>
         </li>
     </ul>
 </nav>
 <!-- /.navbar -->

