 <!-- Navbar Main -->
<nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item d-none d-sm-inline-block">
           <a class="nav-link"> Home </a>
        </li>
        <li class="nav-item">
           <a class="nav-link" href="{{route('login')}}"> Login </a>
        </li>
        <li class="nav-item">
           <a class="nav-link" href="{{route('register')}}"> Register </a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="darkModeSwitch" v-model="isDarkMode">
                <label class="custom-control-label" for="darkModeSwitch">Darkmode</label>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->