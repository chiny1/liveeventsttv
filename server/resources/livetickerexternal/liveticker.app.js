//liver ticker app vue
require("./bootstrap");
import axios from './axios';
import Vue from 'vue';
import LiveTickerExternal from './components/LiveTickerExternal.vue';

const app = new Vue({
    components: { LiveTickerExternal },
    name: 'App',
    el: '#app',
});