//service for deling with authentication 
//and accesKey management to LARAVEL API
import {Util} from './../Util';

/**
 * Retrieve Acces Token for axios
 */
export const AuthenticationService = {
    getAccesToken() {
        return Util.getUrlParam("token");
    }
}