export const Util = {
    getUrlParam(name) {
        let urlParams = new URLSearchParams(window.location.search);
        if(urlParams.has(name)) {
            return urlParams.get(name);
        }
    }
}