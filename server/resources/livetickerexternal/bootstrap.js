const lodash = require('lodash');
const popper = require('popper.js').default;
require('bootstrap');

import Echo from 'laravel-echo';
import { APP_HOST_URL } from './axios';

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'local',
    wsHost: APP_HOST_URL,
    wsPort: 6001,
    forceTLS: true,
    disableStats: true,
    enabledTransports: ['ws', 'wss'],
});