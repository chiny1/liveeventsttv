import axios from 'axios';
import { AuthenticationService } from './services/AuthenticationService';

//test env
export const APP_HOST_URL_PROD = "https://crfweb.uber.space/lens/public";
export const APP_HOST_URL_DEV = "http://localhost/LiveEventTicker/server/public";

//local env
export const APP_HOST_URL = (location.hostname == "localhost") ? APP_HOST_URL_DEV : APP_HOST_URL_PROD;
export const APP_API_URL = "/api/v1";
export const APP_DOMAIN = location.hostname;

axios.defaults.baseURL = APP_HOST_URL + APP_API_URL;
if (axios.defaults.baseURL === "") {
  console.error("No baseUrl set for API calls: Axios config...");
}

axios.defaults.headers.common['Authorization'] = 'Bearer ' + AuthenticationService.getAccesToken();
if (axios.defaults.headers.common['Authorization'] === "") {
  console.error("No accessToken set for API calls: Axios config...");
}