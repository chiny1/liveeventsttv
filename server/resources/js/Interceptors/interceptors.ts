import { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import toastr from 'toastr';
import router from "../app";
let _ = require('lodash');

/**
 * Interceptor: checks for errors and successfull responses.
 * Displays toast message accordingly
*/


/**
 * Successfull request handling
 * 
 * @param config 
 * @returns 
 */
const onRequest = (config: AxiosRequestConfig) : AxiosRequestConfig => {
    return config;
};


/**
 * Unsuccessfull request handling
 * 
 * @param error 
 * @returns 
 */
const onRequestError = (error: AxiosError): Promise<AxiosError> => {
    let message = (error.response?.data.message) || "An error occured during request";
    toastr.error(message, error.message)
    return Promise.reject(error);
}

/**
 * Successfull response handling
 * 
 * @param response 
 * @returns 
 */
const onResponse = (response: AxiosResponse): Promise<AxiosResponse> => {
    if(response.data && response.data?.message.length > 0) {
        toastr.success(response.data.message.toString());
    }
    return Promise.resolve(response);
}

/**
 * Unsuccessfull response handling
 * 
 * @param error 
 * @returns 
 */
const onResponseError = (error: AxiosError): Promise<AxiosError> => {
    let message = (error.response?.data.message) || "An error occured";
    
    //unauthenticated error, go to logout
    if(error.request?.status === 401) {
        //router go logout page
        toastr.info("You may now logout...", "Please Logout correctly" );
        _.debounce( () => {
            toastr.error(message, error.message);
        }, 150 );

        return Promise.reject(error);
    }
    
    if(error.request?.status === 403) {
        let errMsg = message.length > 0 ? message : "Action Not Allowed";
        toastr.error(errMsg, "Access Denied");
        return Promise.reject(error);
    }

    if(error.request?.status === 423) {
        let errMsg = message.length > 0 ? message : "Permission Denied by Server";
        console.warn("Permission denied", errMsg);
        return Promise.reject(errMsg);
    }
    
    toastr.error(message, error.message);
    
    return Promise.reject(error);
}


/**
 * Register interceptors for axios instance
 * 
 * @param axiosInstance 
 * @returns 
 */
export function registerInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
    axiosInstance.interceptors.request.use(onRequest, onRequestError);
    axiosInstance.interceptors.response.use(onResponse, onResponseError);
    
    return axiosInstance;
}


//@TODO add xhr interceptors.. for file uploads