import Vue from 'vue';

export const AppState = Vue.observable({
    isDarkMode: false,
});

export const AppStore = {
    setDarkMode(val) {
        localStorage.setItem('darkMode', val);
        AppState.isDarkMode = val;
    },
    isDarkMode() {
        let isDark = localStorage.getItem('darkMode');
        AppState.isDarkMode = (isDark == "true");
        return AppState.isDarkMode;
    }
}