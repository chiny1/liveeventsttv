import Vue from 'vue';

export const store = Vue.observable({
    tabs: [],
    selectedTab: {}
});

export const Tabs = {
    init(val) {
        store.tabs = val;
        
        //add index
        let index = 0;
        store.tabs.forEach(tab => {
            tab.index = index;
            index++;
        });
    },
    unset() {
        store.tabs = [];
    }
}

export const TabsEventBus = new Vue({
    props: {
        title: String,
    },
    created() {
        this.$on('post-init', this.postInit);
        this.$on('tabs-changed', (index) => this.tabsChanged(index));
    },
    destroyed() {
        this.$off('post-init', this.postInit);
    },
    methods: {
        //tabs changed by user | click on tabs
        tabsChanged(index) {
            this.setActiveTabAndSelectedTab(index);
        },
        //select tab programatically
        selectTab(num) {
            //this.$emit('tabs-changed', num);
            if(!this.tabs.length) {
                throw {
                    name: "NoTabsSpecified",
                    message: "Please init tabs in store before try to access tabs...",
                    toString: function() {
                        return this.name + ": " + this.message;
                    }
                };
            }
            let tab = this.tabs[num];
            $('#' + this.tabId( tab.name )).tab('show');

            this.$emit('tabs-changed', num);
        },
        //update active tab
        setActiveTabAndSelectedTab(num) {
            if(!this.tabs[num]) {
                return;
            }
            
            store.tabs.forEach((tab) => {
                tab.active = false;
            });
            store.tabs[num].active = true;
            store.selectedTab = store.tabs[num];
        },
        getActiveTab() {
            return this.selectTab;
        },
        tabLink(name) {
            return "#"+ this.tabsSelector() +"-"+name.trim();
        },
        tabId(name) {
            return this.tabsSelector()+"-"+name.trim()+"-tab";
        },
        ariaControlls(name) {
            return this.tabsSelector()+"-"+name.trim();
        },
        contentId(name) {
            return this.tabsSelector()+"-"+name.trim();
        },
        contentAriaLabel(name) {
            return this.tabsSelector()+"-"+name.trim()+"-tab";
        },
        hasComponent(tab) {
            return tab.hasOwnProperty('component');
        },
        hasContent(tab) {
            return tab.hasOwnProperty('content');
        },
        containerName(name) {
            return "content"+name.trim();
        },
        hasTabParams(tab) {
            return tab.hasOwnProperty('params');
        },
        tabsSelector() {
            return "v-tabs-"+this.title;
        },
        getTabs() {
            return this.tabs;
        }
    },
    computed: {
        tabs: {
            get: function() {
                return store.tabs;
            },
            set: function(tabs) {
                store.tabs = tabs;
            }
        },
        selectedTab: {
            get: function() {
                return store.selectedTab;
            },
            set: function(val) {
                store.selectedTab = val;
            }
        }
    },
});