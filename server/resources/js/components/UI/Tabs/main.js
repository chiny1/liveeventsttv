import TabLinks from './TabLinks.vue';
import TabContent from './TabContent.vue';
import {TabsEventBus} from './store';

export {
    TabLinks,
    TabContent,
    TabsEventBus,
}

/**/
export default {
    install (Vue) {
        Vue.component('v-tab', TabLinks)
        Vue.component('v-tab-content', TabContent)
    }
}