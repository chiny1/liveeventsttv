import Vue from 'vue';

export const PointTemplateData = Vue.observable({
    showInOut: false,
    fields: [],
});

export const PointsTemplateHelper = {
    toggleShowInOut() {
        PointTemplateData.showInOut = !PointTemplateData.showInOut;
    }
}