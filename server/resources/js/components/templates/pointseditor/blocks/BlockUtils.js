import Vue from 'vue';
import { PointTemplateData } from '../PointTemplateEditor';
import { PointsTemplateData } from '../PointTemplateService';
import PointField from './../PointField.vue';

import BlockCompareGreaterThanVue from './BlockCompareGreaterThan.vue';
import BlockCompareLessThanVue from './BlockCompareLessThan.vue';
import BlockLimitMaxVue from './BlockLimitMax.vue';
import BlockLimitMinVue from './BlockLimitMin.vue';
import BlockOperationMinusVue from './BlockOperationMinus.vue';
import BlockOperationPlusVue from './BlockOperationPlus.vue';

/**
 * Block helper class
 */
export const BlockUtils = {
    updateBlocks(blocks, index) {
        if (index < 0 || !blocks) {
            return;
        }

        if (blocks.length <= 0) {
            blocks = [];
        }

        let fields = PointTemplateData.fields;

        if (fields) {
            fields[index].blocks = blocks;
        }
    },
    addField(fieldData = null) {
        let field = {
            id: 0,
            component: PointField,
            name: "",
            blocks: [],
            uid: ""
        };
        field.uid = this.createUid();

        if (fieldData.name) {
            field.name = fieldData.name;
        }

        if (fieldData.blocks) {
            field.blocks = fieldData.blocks;
        }
        field.id = fieldData.id && fieldData.id > 0 ? fieldData.id : 0;

        PointTemplateData.fields.push(field);

        return field;
    },
    createUid() {
        return Math.random()
            .toString(16)
            .slice(2);
    },
    processBlocks(blocks) {
        let sum = 0;

        if (!blocks) {
            console.error("Block proccessing error: blocks are undefined!");
            sum = 0;
            return sum;
        }

        blocks.forEach(element => {

            //check for Add or Minus blocks data structure
            if (element.sum) {
                sum += element.sum ? element.sum : 0;
            }

            //check for limit operation block data structure
            if (element.limit) {
                let operator = element.limit.operator;
                let operand = parseInt(element.limit.operand);

                if (operator === "MAX") {
                    sum = sum > operand ? operand : sum;
                }

                if (operator === "MIN") {
                    sum = sum < operand ? 0 : sum;
                }
            }
        });

        return sum;
    },
    serializeFields(fields = []) {
        let _fields = [];

        fields.forEach((field) => {
            let _field = {
                id: field.id,
                name: field.fieldName || field.name,
                blocks: [],
            };

            let _blockDataArray = [];
            field.blocks.forEach((block) => {
                let blockData = { ...block };
                let blockComponent = block.component;

                delete blockData.component;
                blockData.component = blockComponent.name;

                _blockDataArray.push(JSON.stringify(blockData));
            });

            _field.blocks = _blockDataArray;
            _fields.push(_field);
        });

        return _fields;
    },
    serializeTemplate(name, id, fieldData) {
        let fields = this.serializeFields(fieldData);
        let data = {
            template_name: name,
            fields: fields,
            id: id
        };

        return data;
    },
    initBlocks(blockData) {
        let processedBlocks = [];

        if (!blockData || blockData.length <= 0) {
            return [];
        }

        blockData.forEach(block => {
            if (block.component === "BlockCompareGreaterThanComponent") {
                block.component = BlockCompareGreaterThanVue;
            }
            if (block.component === "BlockCompareLessThanComponent") {
                block.component = BlockCompareLessThanVue;
            }
            if (block.component === "BlockLimitMaxComponent") {
                block.component = BlockLimitMaxVue;
            }
            if (block.component === "BlockLimitMinComponent") {
                block.component = BlockLimitMinVue;
            }
            if (block.component === "BlockOperationMinusComponent") {
                block.component = BlockOperationMinusVue;
            }
            if (block.component === "BlockOperationPlusComponent") {
                block.component = BlockOperationPlusVue;
            }

            if (typeof block.component === "object") {
                processedBlocks.push(block);
            } else {
                console.warn("unknown block type: " + block.component);
            }
        });

        return processedBlocks;
    },
    parseFields(template) {
        let fields = template.fields;
        if (fields.length > 0) {
            fields.forEach((field) => {
                let parsedBlocks = [];

                field.blocks.forEach((block) => {
                    let _block = JSON.parse(block.data);
                    _block.id = block.id;
                    _block.point_field_id = block.point_field_id;
                    parsedBlocks.push(_block);
                });

                field.blocks = parsedBlocks || [];

                BlockUtils.addField(field);
            });
        }
    }
}