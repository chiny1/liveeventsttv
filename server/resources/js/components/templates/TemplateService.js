import axios from "axios"
import API from "../../api"

export const TemplateService = {
    async getTemplates() {
        return await axios.get(API.TEMPLATES_ALL);
    },
    async loadTemplate(templateId) {
        return await axios.get(API.TEMPLATE_SHOW + templateId);
    },
    async deleteTemplate(templateId) {
        if(templateId > 0) {
            return await axios.delete(API.TEMPLATE_DELETE + templateId);
        }
    },
    async getTemplateForEvent(eventId) {
        if(!eventId || eventId <= 0) {
            console.error("No event Id specified");
            return;
        }
        return await axios.get(API.TEMPLATE_BY_EVENT + eventId);
    }
}