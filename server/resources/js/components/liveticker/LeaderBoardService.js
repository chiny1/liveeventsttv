import axios from "axios"
import API from "../../api"
import Vue from 'vue';
import { APP_API_URL, APP_HOST_URL } from "../../axios";

export const LeaderBoardService = {
    async getSoloStats(eventId) {
        return await axios.get(API.STATS_SOLO + eventId);
    },
    async getTeamStats(eventId) {
        return await axios.get(API.STATS_TEAM + eventId);
    },
    async getEventStats(eventId) {
        return await axios.get(API.STATS_EVENT + eventId);
    },
    async getEventStatsExternal(token) {
        return await axios.get(APP_HOST_URL + APP_API_URL + API.LIVETICKER_STATS_EXTERNAL, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });
    }
}