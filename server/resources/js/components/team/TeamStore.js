import Vue from 'vue';

export const TeamStore = Vue.observable({
    isModify: false,
    params: {
        id: 0,
        action: ""
    },
});

export const TeamStoreMutations = {
    modify(action, id) {
        if(id <= 0 || id == undefined) {
            TeamStore.params.id = 0;
        } else {
            TeamStore.params.id = id;
        }
        TeamStore.params.action = action;
        this.openDialog();
    },
    openDialog() {
        TeamStore.isModify = true;
    },
    closeDialog() {
        TeamStore.isModify = false;
        this.reset();
    },
    reset() {
        TeamStore.params.id = 0;
        TeamStore.params.action = "";
    }
};