import { deprecate } from 'util';
import Vue from 'vue';

/**
 * @deprecated Since version 1.0. Will be deleted in version 3.0. Use bar instead.
 */
export const ACTIONS = {
    "ADD": "add",
    "EDIT": "edit",
};

/**
 * @deprecated Since version 1.0. Will be deleted in version 3.0. Use bar instead.
 */


/**
 * Allowed mutations: ways to manipulate data.
 *  @deprecated 
 */
export const mutations = {
    setIsModify(val) {
        this.state.isModify = val;
    },
    setParams(id, action) {
        this.state.params.id = id;
        this.state.params.action = action;
    },
    setAction(val) {
        this.state.params.action = val;
    },
    setId(val) {
        this.state.params.id = val;
    },
    modify(action, id) {
        state.params.action = action;
        state.params.id = id;
        
        this.openDialog();
    },
    openDialog() {
        state.isModify = true;
    },
    closeDialog() {
        state.isModify = false;
    },
    canModify() {
        //@TODO: add permissions check function like: can('permission_String')... maybe write service class that does that... 
        //DO PERMISSION CHECK via API
        state.canModify = true;
        //FOR NOW allow modify
        return state.canModify;
    },
};