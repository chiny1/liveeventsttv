import axios from "axios"
import API from "../../api"
import Vue from 'vue';

export const SubmissionInfo = Vue.observable({
    fullMatch: {},
    isModify: false,
});

export const SubmissionService = {
    async submitMatch(eventId, fields) {
        return await axios.post(API.MATCH_DATA + eventId, fields);
    },
    async getRecentMatchUser() {
        return await axios.get(API.MATCH_RECENT);
    },
    async getUserMatches() {
        return await axios.get(API.MATCHES_USER);
    },
    async getUserMatchesByEvent(eventId) {
        return await axios.get(API.MATCHES_USER_BY_EVENT + eventId);
    },
    async removeMatchUser(matchId) {
        return await axios.delete(API.MATCH_REMOVE_SELF + matchId);
    },
}