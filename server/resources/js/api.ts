/**
* API Routes
*/
const API = {
    //AUTH
    LOGIN: "/login",
    LOGOUT: "/logout",

    //USER
    USER_PROFILE: "profile",
    USERS_ALL: "users",
    USER_SHOW: "users/",//+id
    USER_STORE: "users",
    USER_UPDATE: "users/",//+id
    USER_UPDATE_PROFILE: "/users/profile/",//+id
    USER_DELETE: "users/",//+id
    USER_EVENTS: "user/events",
    USER_EVENTS_FILTERED: "user/events/filtered",
    USER_SEARCH: "users/search",
    USER_TEAMS: "/user/teams",

    //ORGANIZER
    ORGANIZER_EVENTS: "organizer/events",
    ORGANIZER_UPDATE: "organizers/",//+EventId
    ORGANIZERS_SHOW: "organizers/",//+EventId
    
    //EVENTS
    EVENTS_ALL: "events",
    EVENT_SHOW: "events/",//+id
    EVENT_DETAILS: "events/details/",//+eventId
    EVENT_STORE: "events",
    EVENT_UPDATE: "events/",//+id
    EVENT_DELETE: "events/",//+id
    EVENTS_USER: "events/user/",//+id
    EVENTS_ORGANIZER: "events/organizer/",//+id | Events user is hosting
    EVENT_DOMAINS: "event/domains",
    EVENTS_GLOBAL_PREVIEW_STREAMERS: "events/global/preview/",//+ num of entries
    EVENTS_GLOBAL_PREVIEW: "/events/global/",//+num / +page
    EVENTS_OWNER: "/events/owner/",//+userID
    EVENTS_OWNER_SELF: "/events/owner/self",// of logged in user
    EVENTS_SEARCH: "/search/events",//+searchTerm
    EVENT_TEAM_SUBSCRIBED: "/event/sub/status/team/",//+eventId | retrieve subscribed event for this event
    EVENT_DISQUALIFY_USER: "/event/ban/",//+userId | eventId
    EVENT_UNDISQUALIFY_USER: "/event/unban/",//+userId | eventId
    EVENT_SUB_META: "/event/sub/meta/",//+eventId,

    //EVENT Subscription
    EVENT_CHECK_SUBSCRIPTION: "/event/sub/status/",//+eventId
    EVENT_SUBSCRIBE: "/event/subscribe/",//+eventId | Subscribe user self
    EVENT_UNSUBSCRIBE: "/event/unsubscribe/",//+eventId | Unsubscribe user self

    //TEMPLATES
    EVENT_TEMPLATE_STORE: "/templates/",//+eventId | data
    EVENT_TEMPLATE_UPDATE: "/templates/",//+eventId | +templateId | data
    TEMPLATES_ALL: "/templates",
    TEMPLATE_SHOW: "/templates/",//+templateId
    TEMPLATE_DELETE: "/templates/",//+templateId
    TEMPLATE_BY_EVENT: "/template/event/",//+eventId

    //TEAMS
    TEAM_SHOW: "/teams/",//+id
    TEAM_STORE: "/teams",//+data
    TEAM_DELETE: "/teams/",//+id
    TEAM_UPDATE: "/teams",//+data
    TEAM_MEMBER_ADD: "teams/member/add",//+id
    TEAM_MEMBER_DELETE: "",
    TEAM_SUBSCRIBE: "/event/subscribe/",//+eventId | +teamId
    TEAM_UNSUBSCRIBE: "/event/unsubscribe/",//+eventId | +teamId
    TEAMS_USER: "teams/user/self",//show teams user is assigned to

    //uploads
    UPLOAD_CHANNEL_BANNER: "/upload/channel/banner",
    REMOVE_CHANNEL_BANNER: "/remove/channel/banner",
    UPLOAD_CHANNEL_LOGO: "/upload/channel/logo",
    REMOVE_CHANNEL_LOGO: "/remove/channel/logo",


    //submissions
    MATCH_DATA: "/submission/add/",//+eventId | fields
    MATCH_RECENT: "/submissions/recent/",//eventId
    MATCHES_USER: "/matches",
    MATCHES_USER_BY_EVENT: "/matches/event/",//+eventId
    MATCH_REMOVE_SELF: "/match/remove/",//+matchId

    //PERMISSIONS
    PERMISSION_EVENT_MODIFY: "/permission/modify/event/",//+eventId

    //LEADERBOARD
    STATS_SOLO: "/stats/solo/",//+eventId
    STATS_TEAM: "/stats/team/",//+eventId
    STATS_EVENT: "/stats/event/",//+eventId

    //LIVETICKERS
    LIVETICKERS_ALL: "/livetickers/all",
    LIVETICKERS_EVENT: "/livetickers/",//+eventId
    LIVETICKERS_STORE: "/livetickers/",
    LIVETICKERS_UPDATE: "/livetickers/",//+eventId
    LIVETICKERS_REMOVE: "/livetickers/",
    LIVETICKER_STATS_EXTERNAL: "/liveticker/external/stats",//needs access key
};

export const WEB = {
    LIVETICKER_STATS_EXTERNAL_WEB: "/liveticker/external",//liveticker external URL
}

export default API;