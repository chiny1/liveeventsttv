import axios from 'axios';
import { registerInterceptorsTo } from './Interceptors/interceptors';
import { UserService } from './services/user/UserService';

//test env
export const APP_HOST_URL_PROD = "https://crfweb.uber.space/lens/public";
export const APP_HOST_URL_DEV = "http://localhost/LiveEventTicker/server/public";

//local env
export const APP_HOST_URL = (location.hostname == "localhost") ? APP_HOST_URL_DEV : APP_HOST_URL_PROD;
export const APP_API_URL = "/api/v1";
export const APP_DOMAIN = location.hostname;

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
//set CSRF-TOKEN: Laravel
let token: HTMLMetaElement | null = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
  axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


//set base API URI

axios.defaults.baseURL = APP_HOST_URL + APP_API_URL;
if (axios.defaults.baseURL === "") {
  console.error("No baseUrl set for API calls: Axios config...");
}

//set auhtorization header: token based
axios.defaults.headers.common['Authorization'] = 'Bearer ' + UserService.getAccessTokenLocalStorage();
if (axios.defaults.headers.common['Authorization'] === "") {
  console.error("No accessToken set for API calls: Axios config...");
}


//register interceptors
registerInterceptorsTo(axios);