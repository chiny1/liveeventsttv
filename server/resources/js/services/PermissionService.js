import axios from 'axios';
import api from './../api';

/**
 * Simple permission service:
 * 
 */
export const PermissionService = {
    //Can currently logged in user this event?
    async canModifyEvent(eventId) {
        return await axios.get(api.PERMISSION_EVENT_MODIFY + eventId);
    }
}