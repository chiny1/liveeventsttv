import Vue from 'vue';
import axios from 'axios';
import api from '../../api';

const KEY_ACCESS_TOKEN = "authAccessTokenTTV";

export const UserStore = Vue.observable({
    userData: {},
    userId: 0,
    permissions: [],
    roles: [],
});

export const UserService = {
    storeUserData(data) {
        UserStore.userData = data;
        UserStore.userId = data.id;
        UserStore.permissions = data.roles[0].permissions;
        UserStore.roles = data.roles;
        
        localStorage.setItem('roles', UserStore.roles);
        localStorage.setItem('permissions', UserStore.permissions);
    },
    getAccessToken() {
        return UserService.getAccessTokenLocalStorage();
    },
    getAccessTokenLocalStorage() {
        return localStorage.getItem(KEY_ACCESS_TOKEN);
    },
    removeAccesTokenLocalStorage() {
        localStorage.removeItem(KEY_ACCESS_TOKEN);
        this.updateAxiosDefaultHeaders(); //keep access token updated, if it changes!
    },
    setAccessTokenLocalStorage(token) {
        if(token) {
            localStorage.setItem(KEY_ACCESS_TOKEN, token);
            this.updateAxiosDefaultHeaders(); //keep access token updated, if it changes!
        } else {
            throw {
                'message': "No access token provided for local user!"
            };
        }
    },
    updateAxiosDefaultHeaders() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + UserService.getAccessTokenLocalStorage();
    },
    async fetchUserData() {
        if(UserService.getAccessTokenLocalStorage() !== "") {
            try {
                UserService.getProfile()
                .then((res) => {
                    let data = res.data.data;
                    UserService.storeUserData(data);
                });
            } catch(e) {
                //
            }
        }
    },
    async userSearch(term) {
        return await axios.post(api.USER_SEARCH, {search_term: term,});
    },
    async getAllUsers() {
        return await axios.get(api.USERS_ALL);
    },
    async getProfile() {
        return await axios.get(api.USER_PROFILE);
    },
    async updateProfile(userId, userData) {
        return await axios.put(api.USER_UPDATE_PROFILE + userId, userData);
    },
    /**
     * @deprecated | User EventService.canSubscribe
     */
    async canSubscribe(eventId) {
        return await axios.get(api.EVENT_CHECK_SUBSCRIPTION + eventId);
    }
}
