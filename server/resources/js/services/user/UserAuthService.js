import axios from 'axios';
import { APP_HOST_URL } from '../../axios';
import api from './../../api';

export const UserAuthService = {
    doLogin(credentials) {
        axios.post(api.LOGIN, credentials)
        .then((res) => {
            let data = res.data.data;
            UserData.user = data.user;
        });
    },
    doLogout() {
        let token = $('meta[name="csrf-token"]').attr("content");

        axios({
        method: "post",
        url: api.LOGOUT,
        data: "",
        _token: token,
        baseURL: APP_HOST_URL
        })
        .then((res) => {
            console.log(res);
        })
        .catch((err) => {
            console.warn(err);
        })
        .finally(() => {
            document.location.replace(APP_HOST_URL + api.LOGIN);
        });
    },
}