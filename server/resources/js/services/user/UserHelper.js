import { UserStore } from './UserService';

/**
 * Util class for user
 */
export const UserHelper = {
    getUserId() {
        return UserStore.userId;
    },
    setUserId(id) {
        UserStore.userId = id;
    },
    getRoles() {
        let data = localStorage.getItem('roles');
        return  data ?  data : [];
    },
    getPermissions() {
        let data = localStorage.getItem('permissions');
        return  data ?  data : [];
    },
    can(permToCheck) {
        let can = false;
        UserStore.permissions.forEach((permission) => {
            if(permission.name === permToCheck) {
                can = true;
                return;
            }
        });

        return can;
    },
    isRole(roleToCheck) {
        let isRole = false;
        UserStore.roles.forEach((role) => {
            if(role.name === roleToCheck) {
                isRole = true;
                return;
            }
        });

        return isRole;
    },
};
