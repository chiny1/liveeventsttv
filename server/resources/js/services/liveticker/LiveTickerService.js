import axios from "axios"
import API from "../../api";


export const LiveTickerService = {
    async getAllLiveTickersUser() {
        return await axios.get(API.LIVETICKERS_ALL);
    },
    async getLiveTicker(eventId) {
        return await axios.get(API.LIVETICKERS_EVENT + eventId);
    },
    async storeLiveTicker(eventId) {
        return await axios.post(API.LIVETICKERS_STORE + eventId);
    },
    async updateLiveTicker(eventId) {
        return await axios.put(API.LIVETICKERS_UPDATE + eventId);
    },
    async removeLiveTicker(id) {
        return await axios.delete(API.LIVETICKERS_REMOVE + id);
    }
}