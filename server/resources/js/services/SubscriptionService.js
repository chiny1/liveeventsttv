import axios from "axios"
import API from "../api"


export const SubscriptionService = {
    async getSubMeta(eventId) {
        return await axios.get(API.EVENT_SUB_META + eventId);
    }
}