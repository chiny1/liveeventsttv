import axios from "axios";
import api from './../../api';
import Vue from 'vue';
import { store } from "../../components/UI/Tabs/store";

export const EventMode = {
    SOLO: "solo",
    TEAM: "team",
    BOTH: "both",
};

export const ModifyActions = {
    "ADD": "add",
    "EDIT": "edit",
};

export const EditComponentName = "event-edit";
export const AddComponentName = "event-add";

export const SubData = Vue.observable({
    isModify: false,
    params: {
        id: 0,
        action: "",
        data: null,
    },
    subState: "",
});

export const EventService = {
    checkMode(editCb, addCb, context) {
        let routeName = context.$route.name;

        if (routeName === EditComponentName) {
         return editCb();
        }
  
        if (routeName === AddComponentName) {
            return addCb();
        }
    },
    setParams(eventId, action) {
        SubData.params.id = eventId;
        SubData.params.action = action;
    },
    async subscribeUser(event_id, user_id = 0) {
        if(user_id) {
            //sub specific user
        }

        return await axios.post(api.EVENT_SUBSCRIBE + event_id);
    },
    async unsubcribeUser(event_id, user_id = 0) {
        if(user_id) {
            //unsub specific user
        }

        return await axios.post(api.EVENT_UNSUBSCRIBE + event_id);
    },
    async canSubscribe(eventId) {
        return await axios.get(api.EVENT_CHECK_SUBSCRIPTION + eventId);
    },
    async subscribeTeam(event_id, team_id) {
        return await axios.post(api.TEAM_SUBSCRIBE + event_id +"/" + team_id);
    },
    async unsubcribeTeam(event_id, team_id) {
        return await axios.post(api.TEAM_UNSUBSCRIBE + event_id +"/" + team_id);
    },
    async getSubscribedTeam(eventId) {
        return await axios.get(api.EVENT_TEAM_SUBSCRIBED + eventId);
    },
    openDialog() {
        SubData.isModify = true;
    },
    closeDialog() {
        SubData.isModify = false;
        this.reset();
    },
    isModify() {
        return SubData.isModify;
    },
    reset() {
        SubData.params.id = 0;
        SubData.params.action = "";
    },
    async getAllEvents() {
        return await axios.get(api.EVENTS_ALL);
    },
    async getUserEventsOwner() {
      return await axios.get(api.EVENTS_OWNER_SELF);
    },
    async getOrganizerEvents() {
        return await axios.get(api.ORGANIZER_EVENTS);
    },
    async getUserEvents() {
        return await axios.get(api.USER_EVENTS);
    },
    async getUserEventsFiltered() {
        return await axios.get(api.USER_EVENTS_FILTERED);
    },
    async show(id) {
        return await axios.get(api.EVENT_SHOW + id);
    },
    async getDomains() {
        return await axios.get(api.EVENT_DOMAINS);
    },
    async update(eventId, eventData) {
        return await axios.put(api.EVENT_UPDATE + eventId, eventData);
    },
    async store(eventData) {
        return await axios.post(api.EVENT_STORE, eventData);
    },
    async getUserSubScription() {
        return await axios.get(api.USER_EVENT_SUBSCRIPTION);
    },
    async search(term) {
        return await axios.post(api.EVENTS_SEARCH, {search_term: term});
    },
    async getEventById(eventId) {
        return await axios.get(api.EVENT_DETAILS + eventId);
    },
    async updateEventTemplate(eventId, templateId, data) {
        return await axios.put(api.EVENT_TEMPLATE_UPDATE + eventId + "/" + templateId, data);
    },
    async storeEventTemplate(eventId, data) {
        return await axios.post(api.EVENT_TEMPLATE_STORE + eventId, data);
    },
    async disqualify(matchId) {
        return await axios.post(api.EVENT_DISQUALIFY_USER + matchId);
    },
    async unDisqualify(matchId) {
        return await axios.delete(api.EVENT_UNDISQUALIFY_USER + matchId);
    },
}
