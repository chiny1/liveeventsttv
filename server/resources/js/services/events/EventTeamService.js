import axios from "axios"
import api from './../../api';

/**
 * Event Organizer Service
 */
export const EventTeamService = {
    async store(eventId, members) {
        if(eventId <= 0) { return;}
        return await axios.post(api.ORGANIZER_UPDATE + eventId, {members: members});
    },
    async show(eventId) {
        if(eventId <= 0) { return;}
        return await axios.get(api.ORGANIZERS_SHOW + eventId);
    }
};