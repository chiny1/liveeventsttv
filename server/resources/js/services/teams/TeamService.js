import axios from "axios"
import api from "../../api"

/**
 * Team Service class
 */
export const TeamService = {
    //get teams of user | leading teams
    async getTeamsUser() {
        return await axios.get(api.TEAMS_USER);
    },
    //Get user teams | assigned teams
    async getTeams() {
        return await axios.post(api.USER_TEAMS);
    }
}