import {
    APP_HOST_URL
} from "../axios";
const _ = require('lodash');
import Router from 'vue-router';
import router from "../app";
/**
 * Utils class | simple helper methods
 */
export const Utils = {
    getTitle(componentName, action) {
        let title = " ";
        try {
            title +=
                action.charAt(0).toUpperCase() +
                action.slice(1) +
                " " + componentName;
        } catch (e) {}
        return title;
    },
    convertTimestampToDate(timestamp) {
        return new Date(timestamp).toLocaleDateString('de-DE');
    },
    convertTimestampToTime(timestamp) {
        return new Date(timestamp).toLocaleTimeString('de-DE', {hour: 'numeric', minute: 'numeric'});
    },
    convertTimestampToDateTime(timestamp) {
        let date = this.convertTimestampToDate(timestamp);
        let time = this.convertTimestampToTime(timestamp);

        return date + " - " + time;
    },
    setChannelBg(imgURL, previewId) {
        if (imgURL) {
            let image = Utils.asset(imgURL);
            document.getElementById(previewId).style.background = "url('" + image + "') center center";
            document.getElementById(previewId).style.backgroundSize = "cover";
        }
    },
    openChannel(id) {
        router.push({
            name: 'channel',
            params: {
                id: id
            }
        });
    },
    openEvent(id) {
        if(id <= 0) {
            return;
        }
        let open = function () {
            router.push({
                name: 'event-details',
                params: {
                    id: id
                }
            });
        };
        this.debounce(open(), 50);
    },
    openTeams() {
        let open = function () {
            router.push({name: 'my-team'});
        };
        this.debounce(open(), 50);
    },
    openSubmissionHistory(eventId) {
        router.push({name: 'submission-history', params: {event: eventId}});
    },
    editEvent(id) {
        let open = function () {
            router.push({
                name: 'event-edit',
                params: {
                    id: id
                }
            });
        };
        this.debounce(open(), 50);
    },
    addEvent() {
        let open = function () {
            router.push({name: 'event-add'});
        };
        this.debounce(open(), 50);
    },
    wrapArray(object) {
        let arr = [];
        arr.push(object);
        return arr;
    },
    channelLink(id) {
        return "channel/" + id;
    },
    twitchChannelLink(channelName) {
        if (channelName) {
            return "https://twitch.tv/" + channelName;
        } else {
            return "https://twitch.tv/";
        }
    },
    asset(uri) {
        return APP_HOST_URL + "/" + uri;
    },
    debounce(fn, delay) {
        let timer;
        return function () {
            clearTimeout(timer);
            timer = setTimeout(() => {
                fn();
            }, delay);
        }
    },
    arrayLimit(arr, maxItemsNum) {
        if (arr.length > maxItemsNum) {
            arr.splice(maxItemsNum - 1, arr.length - maxItemsNum);
        }
        return arr;
    },
    createUid() {
        return '_' + Math.random().toString(36).substr(2, 9);
    },
    uuid(e) {
        if (e.uid) return e.uid;
  
        const key = Math.random()
          .toString(16)
          .slice(2);
  
        this.$set(e, "uid", key);
        
        return e.uid;
      }
};
