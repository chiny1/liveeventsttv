require("./bootstrap");
require("./axios");
import Vue from 'vue';
import VueRouter from "vue-router";
import UserComponent from "./components/user/UserComponent.vue";
import EventComponent from "./components/event/EventComponent.vue";
import TeamComponent from './components/team/TeamComponent.vue';
import InviteTeamComponent from './components/team/InviteTeamComponent.vue';
import SmartTable from 'vuejs-smart-table';
import Breadcrumbs from './components/breadcrumbs/BreadCrumbs.vue';
import MyLiveTickerComponent from './components/liveticker/MyLiveTickerComponent.vue';
import LiveTickerComponent from './components/liveticker/LiveTickerComponent.vue';
import Datetime  from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';
import VueSimpleAlert from "vue-simple-alert";
import LogoutComponent from './components/auth/LogoutComponent.vue';
import LoginComponent from './components/auth/LoginComponent.vue';
import ProfileComponent from './components/user/ProfileComponent.vue';
import ChannelComponent from './components/channel/ChannelComponent.vue';
import { TabLinks } from "./components/UI/Tabs/main";
import EventComponentGlobal from './components/event/EventComponentGlobal.vue';
import {AppState, AppStore} from './components/UI/App/AppStore';
import {UserService, UserStore} from './services/user/UserService';
import {UserHelper} from './services/user/UserHelper';
import { EventService } from './services/events/EventService';
import EventSearchResponseListComponent from './components/event/EventSearchResponseListComponent.vue';
import * as _ from 'lodash';
import { Utils } from './utils/utils';
import EventEditorComponent from './components/event/EventEditorComponent.vue';
import EventInfoComponent from './components/event/EventInfoComponent.vue';
import EventAddComponent from './components/event/EventAddComponent.vue';
import SubmissionsComponent from './components/submissions/SubmissionsComponent.vue';
import SubmissionHistoryComponent from './components/submissions/SubmissionsHistoryComponent.vue';
import TemplateComponent from './components/templates/TemplatesComponent.vue';
import { RouteData } from './services/RouteData';

//register plugins
Vue.use(VueRouter);
Vue.use(SmartTable);
Vue.use(Datetime);
Vue.use(VueSimpleAlert);

export const router = new VueRouter({
    routes: [
        {
            name: "logout",
            path: "/logout",
            component: LogoutComponent
        },
        {
            name: "profile",
            path: "/profile",
            component: ProfileComponent,
            meta: {
                pageName: "Profile",
            }
        },
        {
            name: "myChannel",
            path: "/channel",
            component: ChannelComponent,
            meta: {
                pageName: "Your Channel",
            }
        },
        {
            name: "channel",
            path: "/channel/:id",
            component: ChannelComponent,
            meta: {
                pageName: "Channel Info",
            }
        },
        {
            name: "app",
            path: "/",
            redirect: "/events/global",
            meta: {
                pageName: "Global Live Events",
              }
              
        },
        {
            name: "events",
            component: EventComponentGlobal,
            path: "/events/global",
            meta: {
                pageName: "Global Live Events",
            }
        },
        {
            name: "my-team",
            path: "/event/team",
            component: TeamComponent,
            meta: {
                pageName:"My Teams",
            }
        },
        {
            name: "team-invite",
            path: "/team/invite",
            component: InviteTeamComponent,
            meta: {
                pageName:"Invite Team Members",
            }
        },
        {
            name: "my-events",
            path: "/event/own",
            component: EventComponent,
            meta: {
                pageName:"My Events",
            }
        },
        {
            name: "event-details",
            path: "/event/details/:id",
            component: EventInfoComponent,
            meta: {
                pageName:"Event Summary",
            }
        },
        {
            name: "event-edit",
            path: "/event/edit/:id",
            component: EventEditorComponent,
            meta: {
                pageName:"Change Event",
            }
        },
        {
            name: "event-add",
            path: "/event/add",
            component: EventAddComponent,
            meta: {
                pageName:"Create New Event",
            }
        },
        {
            name: "template-add",
            path: "/templates/add",
            component: TemplateComponent,
            meta: {
                pageName:"Points Calculation Editor",
            }
        },
        {
            name: "submissions",
            path: "/submissions",
            component: SubmissionsComponent,
            meta: {
                pageName:"Submit Match Details",
            }
        },
        {
            name: "submission-history",
            path: "/submissions/history/:event",
            component: SubmissionHistoryComponent,
            meta: {
                pageName:"Your Submissions",
            }
        },
        {
            name: "submissions-history",
            path: "/submissions/history",
            component: SubmissionHistoryComponent,
            meta: {
                pageName:"Your Submissions",
            }
        },
        {
            name: "users",
            path: "/user",
            component: UserComponent,
            meta: {
                pageName: "User Administration",
            }
        },
        {
            name: "livetickers",
            component: LiveTickerComponent,
            path: "/live-ticker/global",
            meta: {
                pageName: "Global Live Tickers",
            }
        },
        {
            name: "my-liveticker",
            path: "/live-ticker/own",
            component: MyLiveTickerComponent,
            meta: {
                pageName: "My Live Ticker",
            }
        }

    ],
    linkActiveClass: "active",
});
export default router;


const app = new Vue({
    name: 'App',
    el: "#app",
    router,
    components: {
        Breadcrumbs,
        TabLinks,
        LoginComponent,
        LogoutComponent,
        EventSearchResponseListComponent
    },
    data() {
        return {
            darkMode: false,
            searchTerm: "",
            searchResult: [],
            isSearching: false,
        }
    },
    created() {
        this.darkMode = AppStore.isDarkMode();
        UserService.fetchUserData();
    },
    methods: {
        can(perm: any) {
            return UserHelper.can(perm);
        },
        async search() {
            this.isSearching = true;
            this.searchResult = [];

            if(!this.searchTerm || this.searchTerm.length <= 3) {
                this.isSearching = false;
                return;
            }

            EventService.search(this.searchTerm).then((res) => {
                let data = res.data.data;
                this.searchResult = data;

                this.searchResult = Utils.arrayLimit(this.searchResult, 5);
                
            }).finally(() => this.isSearching = false);

        },
        clearSearchTerm() {
            this.searchTerm = "";
            this.searchResult = [];
        }
    },
    computed: {
        isDarkMode: {
            get() {
                return AppState.isDarkMode;
            },
            set(val) {
                AppStore.setDarkMode(val);
            }
        },
        isAdmin(): boolean {
            let data = UserHelper.isRole("admin")
            return data;
        }
    },
    watch: {
        darkMode(val) {
            AppStore.setDarkMode(val);
        },
        searchTerm() {
            Utils.debounce(this.search(), 300);
        },
        $route (to, from) {
            RouteData.route = this.$route;
        }
    },
});