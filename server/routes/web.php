<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LiveTickerController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', function() {
    if(Auth::check()) {
        return redirect()->route('app');
    }

    return redirect()->route('login');
});

Route::get('/logout', function () {
    return redirect()->route('login');
});

Route::group(['middleware' => ['web', 'auth', 'verified']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/storeAccessKey', [App\Http\Controllers\HomeController::class, 'storeAccessKeySPA'])->name('store.accessKey');
    Route::get('/removeAccessKey', [App\Http\Controllers\HomeController::class, 'removeAccessKeySPA'])->name('remove.accessKey');
    Route::get('/app', [AppController::class, 'index'])->name('app');

    //remove
    Route::get('/users/{user}', function(User $user) {
        return response()->json($user);
    });
});

//liveticker external
Route::get('/liveticker/external', [LiveTickerController::class, 'index']);