<?php

use App\Http\Controllers\ApiLoginController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\LeaderBoardController;
use App\Http\Controllers\LiveTickerController;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\OrganizerController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//additionnal routes for api login
Route::group(['middleware' => ['cors', 'json.response']], function() {

    //public routes
    Route::post('/register', [ApiLoginController::class, 'register']);
    Route::post('/login', [ApiLoginController::class, 'login']);
});


//protected routes | API
Route::group(['middleware' => ['cors', 'json.response', 'auth:api'], 'prefix' => 'v1'], function() {
    //AUTH
    Route::post('/logout', [ApiLoginController::class, 'logout']);

    //USER
    Route::get('/profile', [UserController::class, 'profile']);
    Route::get('/users', [UserController::class, 'index']);
    Route::get('users/{user}', [UserController::class, 'show']);
    Route::post('/users', [UserController::class, 'store']);
    Route::put('/users/{user}', [UserController::class, 'update']);
    Route::put('/users/profile/{user}', [UserController::class, 'updateProfile']);
    Route::delete('/users/{user}', [UserController::class, 'delete']);
    Route::get('/user/events', [UserController::class, 'events']);
    Route::get('/user/events/filtered', [UserController::class, 'eventsFiltered']);
    Route::post('/users/search', [UserController::class, 'search']);
    Route::post('/user/teams', [UserController::class, 'teams']);
    Route::get('/user/permissions', [UserController::class, 'permissions']);
    
    //ORGANIZER
    Route::get('/organizer/events', [OrganizerController::class, 'events']);
    Route::get('organizers/{event}', [OrganizerController::class, 'organizers']);
    Route::post('/organizers/{event}', [OrganizerController::class, 'update']);

    //EVENTS
    Route::get('/events', [EventController::class, 'index']);
    Route::get('events/{event}', [EventController::class, 'show']);
    Route::get('events/details/{event}', [EventController::class, 'showFull']);
    Route::post('/events', [EventController::class, 'store']);
    Route::put('/events/{event}', [EventController::class, 'update']);
    Route::delete('/events/{event}', [EventController::class, 'delete']);
    Route::get('/events/user/{user}', [EventController::class, 'userEvents']);
    Route::get('/event/domains', [EventController::class, 'domains']);
    Route::get('/event/sub/meta/{event}', [EventController::class, 'subMeta']);

    //TEMPLATES
    Route::get('/templates', [TemplateController::class, 'index']);
    Route::get('/templates/{pointTemplate}', [TemplateController::class, 'show']);
    Route::put('/templates/{event}/{pointTemplate}', [TemplateController::class, 'update']);
    Route::post('/templates/{event}', [TemplateController::class, 'store']);
    Route::delete('/templates/{pointTemplate}', [TemplateController::class, 'delete']);
    Route::get("/template/event/{event}", [TemplateController::class, 'templateByEvent']);
    
    //ORGANIZER
    Route::get('/events/organizer/{user}', [EventController::class, 'organizerEvents']);
    Route::get('/events/global/preview/{num}', [EventController::class, 'eventPreviewGlobal']);
    Route::get('/events/owner/self', [EventController::class, 'ownerSelf']);
    Route::get('/events/owner/{user}', [EventController::class, 'owner']);

    //SUBSCRIPTIONS
    Route::get('/event/sub/status/{event}', [EventController::class, 'subStatus']);
    Route::post('/event/subscribe/{event}', [EventController::class, 'subscribe']);
    Route::post('/event/unsubscribe/{event}', [EventController::class, 'unSubscribe']);
    Route::post('/event/subscribe/{event}/{team}', [EventController::class, 'subscribeTeam']);
    Route::post('/event/unsubscribe/{event}/{team}', [EventController::class, 'unSubscribeTeam']);
    Route::get('/event/sub/status/team/{event}', [EventController::class, 'teamSubscribed']);
    Route::post('/event/ban/{matchdata}', [EventController::class, 'ban']);
    Route::delete('/event/unban/{matchdata}', [EventController::class, 'unBan']);

    //SEARCH
    Route::post('/search/events', [EventController::class, 'search']);

    //TEAM
    Route::get('/teams', [TeamController::class, 'index']);
    Route::get('teams/{team}', [TeamController::class, 'show']);
    Route::post('/teams', [TeamController::class, 'store']);
    Route::put('/teams', [TeamController::class, 'update']);
    Route::delete('/teams/{team}', [TeamController::class, 'delete']);
    Route::get('/teams/user/self', [TeamController::class, 'teamsSelf']);

    //UPLOADS
    Route::post('/upload/channel/banner', [UploadController::class, 'channelBanner']);
    Route::delete('/remove/channel/banner', [UploadController::class, 'removeChannelBanner']);
    Route::post('/upload/channel/logo', [UploadController::class, 'channelLogo']);
    Route::delete('/remove/channel/logo', [UploadController::class, 'removeChannelLogo']);

    //SUBMISSIONS
    Route::post('/submission/add/{event}', [MatchController::class, 'match']);
    Route::get('/submissions/recent', [MatchController::class, 'recent']);
    
    //MATCH
    Route::get('/matches', [MatchController::class, 'all']);
    Route::get('/matches/{event}', [MatchController::class, 'matches']);
    Route::get('/matches/event/{event}', [MatchController::class, 'matchesByEvent']);
    Route::delete('/match/remove/{matchdata}', [MatchController::class, 'removeMatchSelf']);

    //PERMISSIONS
    Route::get('/permission/modify/event/{event}', [PermissionController::class, 'modifyEvent']);

    //LEADERBOARD
    Route::get('/stats/solo/{event}', [LeaderBoardController::class, 'solo']);
    Route::get('/stats/team/{event}', [LeaderBoardController::class, 'team']);
    Route::get('/stats/event/{event}', [LeaderBoardController::class, 'event']);

    //LIVETICKERS
    Route::get('/livetickers/all', [LiveTickerController::class, 'livetickersAll']);
    Route::get('/livetickers/{event}', [LiveTickerController::class, 'liveticker']);
    Route::post('/livetickers/{event}', [LiveTickerController::class, 'store']);
    Route::put('/livetickers/{event}', [LiveTickerController::class, 'update']);
    Route::delete('/livetickers/{liveticker}', [LiveTickerController::class, 'delete']);
});

//liveticker external
Route::group(['middleware' => ['auth:api', 'scope:check-stats'], 'prefix' => 'v1'], function() {
    Route::get('/liveticker/external/stats', [LiveTickerController::class, 'stats']);
});