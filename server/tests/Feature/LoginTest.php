<?php

namespace Tests\Feature\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Support\Str;

class LoginTest extends TestCase
{

    public function testRequiresEmailAndLogin()
    {
        $this->postJson('api/login')
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "The email field is required.",
                    "The password field is required."
                ]
            ]);
    }


    public function testUserLoginsSuccessfully()
    {
        $email =  Str::random(10)."@web.de";
        $user = User::factory()->create([
            'email' => $email,
            'password' => Hash::make('toptal123'),
        ]);

        $payload = [
            'email' => $email,
            'password' => 'toptal123',
            'password_confirmation' => 'toptal123',
        ];

        $response = $this->json('POST', 'api/login', $payload);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'token',
                'user'
            ]
        ]);

        $user->delete();
    }
}
