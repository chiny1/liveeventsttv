<?php

namespace Tests\Feature\Feature;

use \App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase as LaravelFeatureTestCase;
use Illuminate\Support\Str;

class LogoutTest extends LaravelFeatureTestCase
{

    public function testUserIsLoggedOutProperly()
    {
        $user = User::factory()->create(['email' => Str::random(10).'@test.com']);
        $repo = new UserRepository();
        $token = $repo->generateToken($user)->accessToken;
        $user->withAccessToken($token);

        $headers = ['Authorization' => "Bearer $token"];

        $this->assertTrue($user->tokens()->count() > 0);
        $this->assertEquals($token, $repo->getAccessToken($user));

        $this->json('get', '/api/users', [], $headers)->assertStatus(200);
        $this->json('post', '/api/logout', [], $headers)->assertStatus(200);

        $user = User::find($user->id);

        $this->assertTrue($user->tokens->count() == 0);
        $this->assertEquals(null, $repo->getAccessToken($user));

        $user->delete();
    }

    public function testUserWithNullToken()
    {
        // Simulating login
        $user = User::factory()->create(['email' => Str::random(12).'@test.com']);
        $repo = new UserRepository();
        $token = $repo->generateToken($user)->accessToken;
        $headers = ['Authorization' => "Bearer $token"];

        //simulate logout
        $repo->removeAccessTokens($user);

        $response = $this->json('get', 'api/users', [], $headers);
        $response->assertStatus(401);
        $user->delete();
    }
}
