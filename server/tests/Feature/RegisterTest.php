<?php

namespace Tests\Feature\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Str;

class RegisterTest extends TestCase
{

    public function testsRegistersSuccessfully()
    {
        $email = Str::random(12) . '@toptal.com';
        $payload = [
            'name' => 'John',
            'email' => $email,
            'password' => 'toptal123',
            'password_confirmation' => 'toptal123',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'token',
                    'user'
                ]
            ]);

        $user = User::where('email', '=', $email)->first();
        $user->delete();
    }

    public function testsRequiresPasswordEmailAndName()
    {
        $response = $this->json('post', '/api/register');
        $response->assertStatus(422);
        $response->assertJson([
            "errors" => [
                "The name field is required.",
                "The email field is required.",
                "The password field is required."
            ]
        ]);
    }

    public function testsRequirePasswordConfirmation()
    {
        $payload = [
            'name' => 'John',
            'email' => 'john@toptal.com',
            'password' => 'toptal123',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "The email has already been taken.",
                    "The password confirmation does not match."
                ]
            ]);
    }
}
