<?php


namespace App\DTO;

use DateTime;
use Spatie\DataTransferObject\DataTransferObject;

class UserEventDTO extends DataTransferObject {
    public int $id;
    public DateTime $start;
    public DateTime $end;
    public String $summary;
    public String $eventMode;
    public String $title;
    public String $visibility;

    public int $participants;
    public int $teams;

    
}