<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserEventsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'end' => $this->end_date,
            'start' => $this->start_date,
            'id' => $this->event_id,
            'mode' => $this->name,
            'summary' => $this->event_description,
            'title' => $this->title,
            'visibility' => $this->visibility,
            'num_participants' => $this->participants,
            'num_teams' => 0, //@TODO: change this when available,
            'event_mode_id' => $this->event_mode,
            'event_vis_id' => $this->vis_id,
        ];
    }
}
