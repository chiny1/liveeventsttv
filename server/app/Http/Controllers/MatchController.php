<?php

namespace App\Http\Controllers;

use App\Events\StatsChangedEvent;
use App\Exceptions\ApiException;
use App\Helper\ApiResponse;
use App\Models\Event;
use App\Models\MatchData;
use App\Services\MatchService;
use App\Services\PermissionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MatchController extends Controller
{
    protected $api;
    protected $matchService;
    protected $permissions;


    public function __construct(MatchService $matchService, PermissionService $perms)
    {
        $this->matchService = $matchService;
        $this->permissions = $perms;
        $this->api = new ApiResponse();
    }

    public function all(Request $request) {
        $user = Auth::guard('api')->user();

        return $this->api->success($this->matchService->userMatchesAll($user));
    }

    public function match(Request $request, Event $event) {
        $data = $request->all();
        
        if(!$data['fields'] || count($data['fields']) == 0) {
            throw new ApiException("Can not save Match data. No input data");
        }

        $user = Auth::guard('api')->user();
        $submission = $this->matchService->addMatch($event, $data, $user);

        //broadcast stats updated
        StatsChangedEvent::dispatch($event);
        
        return $this->api->success($submission, ["Submission received"]);
    }

    public function recent(Request $request) {
        $user = Auth::guard('api')->user();

        return $this->api->success($this->matchService->recent($user));
    }

    public function matches(Request $request) {
        $user = Auth::guard('api')->user();
        
        return $this->api->success($this->matchService->userMatchesAll($user));
    }
    public function matchesByEvent(Request $request, Event $event) {
        $user = Auth::guard('api')->user();
        
        if(!$this->permissions->canModifyEvent($event, $user)) {
            throw new ApiException("You don´t have permission to access match data for this event!");
        }
        
        return $this->api->success($this->matchService->allMatchesByEvent($user, $event));
    }

    public function removeMatchSelf(Request $request, MatchData $matchData) {
        $user = Auth::guard('api')->user();

        if($this->matchService->removeMatch($user, $matchData)) {
            return $this->api->success([], ["Match removed"]);
        } else {
            return $this->api->error([], ["Could not remove match data."]);
        }
    }
}
