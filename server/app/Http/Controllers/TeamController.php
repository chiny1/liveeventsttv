<?php

namespace App\Http\Controllers;

use App\Helper\ApiResponse;
use App\Models\Team;
use App\Models\User;
use App\Repositories\interfaces\TeamRepositoryInterface;
use App\Repositories\TeamRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{   
    protected $api;
    protected $teamRepo;

    public function __construct(TeamRepositoryInterface $team)
    {
        $this->teamRepo = $team;
        $this->api = new ApiResponse();
    }

    public function show(Request $request, Team $team) {
        return $this->api->success($this->teamRepo->team($team));
    }


    /**
     * Save new Team
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'team_name' => "required|string|min:3|max:60|unique:teams,name",
            'team_members' => "required|array|min:1|max:10"
        ]);
        
        if($validator->fails()) {
            return $this->api->error(null, $validator->errors()->all());
        }

        $user = Auth::guard('api')->user();
        
        return $this->api->success($this->teamRepo->createTeam($user, $request->all()), [__('Team created')]);
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'team_name' => "required|string|min:3|max:60",
            'team_members' => "required|array|min:1|max:10"
        ]);
        
        if($validator->fails()) {
            return $this->api->error(null, $validator->errors()->all());
        }

        $user = Auth::guard('api')->user();
        
        $teamId = $request['id'];
        $team = Team::findOrFail($teamId);

        return $this->api->success($this->teamRepo->updateTeam($user, $team, $request->all()), [__('Team created')]);
    }

    public function delete(Team $team) {
        $user = Auth::guard('api')->user();
        
        $this->teamRepo->deleteTeam($team, $user);

        return $this->api->delete(['Team deleted']);
    }

    public function teamsSelf() {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->teamRepo->teamsUser($user));
    }
}
