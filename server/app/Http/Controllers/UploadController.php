<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

define('STORAGE_IMAGE_PATH', 'images');

class UploadController extends Controller
{
   
    public function channelBanner(Request $request) {
        $this->validateImage($request);

        $authenticable = Auth::guard('api')->user();
        $path = $request->file('file')->store(STORAGE_IMAGE_PATH);
        
        $user = User::find($authenticable->id);
        if($user->channel_banner) {
            Storage::delete($user->channel_banner);
        }
        $user->channel_banner = $path;
        $user->save();
    }

    public function removeChannelBanner() {
        $auth = Auth::guard('api')->user();
        $user = User::find($auth->id);

        if($user->channel_banner) {
            Storage::delete($user->channel_banner);
        }
        $user->channel_banner = null;
        $user->save();
    }

    public function channelLogo(Request $request) {
        $this->validateImage($request);

        $authenticable = Auth::guard('api')->user();
        $path = $request->file('file')->store(STORAGE_IMAGE_PATH);
        
        $user = User::find($authenticable->id);
        if($user->logo) {
            Storage::delete($user->logo);
        }
        $user->logo = $path;
        $user->save();
    }

    public function removeChannelLogo() {
        $auth = Auth::guard('api')->user();
        $user = User::find($auth->id);

        if($user->logo) {
            Storage::delete($user->logo);
        }
        $user->logo = null;
        $user->save();
    }

    private function validateImage(Request $request) {
        $this->validate($request, [
            'file' => 'mimes:jpeg,jpg,png,|max:2048',
        ],
            $messages = [
                'required' => 'The :attribute field is required.',
                'mimes' => 'Only jpeg and png are allowed.'
            ]
        );
    }
}
