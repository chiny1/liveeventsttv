<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Helper\ApiResponse;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Models\MatchData;
use App\Models\Team;
use App\Models\User;
use App\Repositories\interfaces\BanRepositoryInterface;
use App\Repositories\interfaces\EventRepositoryInterface;
use App\Services\EventService;
use App\Services\PermissionService;
use App\Services\SubscriptionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    private $eventRepo;
    private $api;
    private $banRepo;
    private $permissionService;
    private $subService;
    private $eventService;

    public function __construct(EventRepositoryInterface $eventRepo, BanRepositoryInterface $ban, PermissionService $perms, SubscriptionService $subs, EventService $events)
    {
        $this->eventRepo = $eventRepo;
        $this->api = new ApiResponse();
        $this->banRepo = $ban;
        $this->permissionService = $perms;
        $this->subService = $subs;
        $this->eventService = $events;
    }
    
    /**
     * Get every Event 
     * 
     * @return void
     */
    public function index()
    {
        return $this->api->success($this->eventRepo->allEvents());
    }

    /**
     * Create new event
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $validator = $this->validateEvent($request);

        if($validator->fails()) {
            return $this->api->error(null, $validator->errors()->all());
        }
        
        $user = Auth::guard('api')->user(); //user that made the request

        $event = $this->eventRepo->store($request->all(), $user);
        
        return $this->api->success($event, [__('Event created')] );
    }
  
    public function show(Event $event)
    {
        return $this->api->success($this->eventRepo->singleEvent($event));
    }

    public function showFull(Event $event) {
        return $this->api->success($this->eventRepo->singleEventFull($event));
    }
    
    public function update(Request $request, Event $event)
    {
        $validator = $this->validateEvent($request);
        
        if($validator->fails()) {
            return $this->api->error(null, $validator->errors()->all());
        }

        $user = Auth::guard('api')->user();

        //permisison check here
        $this->permissionService->validateEventPermissions($event, $user, $request->all());

        //check if is private -> if yes remove all subsc and update
        if($event->isPrivate()) {
            //remove subs so the event can be updated
            $this->subService->removeSubscriptionsForEvent($event);
        }

        return $this->api->success($this->eventRepo->updateEvent($event, $request->all(), $user), [__('Event updated')]);
    }

    public function destroy(Event $event)
    {
        return $this->api->delete([__('Event removed')]);
    }

    /**
     * Retrieve all events a user is registered to
     *
     * @param User $user
     * @return void
     */
    public function userEvents(User $user) {
        if(!$user->exists) {
            return $this->api->error(null, [__('Bad user authentication')]);
        }
        
        return $this->api->success($this->eventRepo->userEvents($user));
    }

    /**
     * Retrieve all events a user is an organizer at | (isHosting event)
     *
     * @param User $user
     * @return void
     */
    public function organizerEvents(User $user) {
        if(!$user->exists) {
            return $this->api->error(null, [__('Bad user authentication')]);
        }

        return $this->api->success($this->eventRepo->organizerEvents($user));
    }

    public function domains() {
        return $this->api->success($this->eventRepo->domains());
    }

    private function validateEvent(Request $request) {
        return Validator::make($request->all(), [
            'title' => 'required|string|min:5|max:80',
            'start_date' => 'required|date|before:end_date',
            'end_date' => 'required|date|after:start_date',
            'event_description' => 'required|string|min:10|max:500',
            'event_mode' => 'required|exists:event_modes,id',
            'vis_id' => 'required|exists:event_visibilities,id',
        ]);
    }

    /**
     * Get random streamer event preview data
     *
     * @param Request $request
     * @return void
     */
    public function eventPreviewGlobal(Request $request, int $num) {
        return $this->api->success($this->eventService->eventPreviewRandom($num));
    }

    public function owner(Request $request, User $user) {
        return $this->api->success($this->eventRepo->ownerEvents($user));
    }

    public function ownerSelf(Request $request) {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->eventRepo->ownerEvents($user));
    }

    public function search(Request $request) {
        $term = (isset($request['search_term'])) ? $request['search_term'] : null;
        if($term) {
            return $this->api->success($this->eventService->search($term));
        } else {
            return $this->api->error([], ['no search term provided']);
        }
    }

    public function subStatus(Request $request, Event $event) {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->subService->checkSubScriptionStatus($event, $user));
    }

    public function subscribe(Request $request, Event $event) {
        $user = Auth::guard('api')->user();
        
        if($this->subService->subscribe($event, $user)) {
            return $this->api->success(null, ['You are now subscribed']);
        } else {
            return $this->api->error(null, ['Ooops, we´re sorry. We can not subscribe you to this event.']);
        }
    }

    public function unSubscribe(Request $request, Event $event) {
        $user = Auth::guard('api')->user();
        
        if($this->subService->unSubscribe($event, $user)) {
            return $this->api->success(null, ['You are now unsubscribed']);
        } else {
            if(!$this->subService->isSubscribed($event, $user)) {
                return $this->api->success(null, ['You are already unsubscribed...']);
            }
            return $this->api->error(null, ['Ooops, we´re sorry. We can not unsubscribe you to this event.']);
        }
    }


    public function subscribeTeam(Request $request, Event $event, Team $team) {
        $user = Auth::guard('api')->user();

        if($this->subService->subscribeTeam($event, $team, $user)) {
            return $this->api->success(null, ['Your team is now subscribed']);
        } else {
            if($this->subService->isSubscribedTeam($event, $team)) {
                return $this->api->success(null, ['Your team is already subscribed...']);
            }
            return $this->api->error(null, ['Ooops, we´re sorry. We can not unsubscribe your team from this event.']);
        }
    }

    public function unSubscribeTeam(Request $request, Event $event, Team $team) {
        $user = Auth::guard('api')->user();

        if($this->subService->unSubscribeTeam($event, $team, $user)) {
            return $this->api->success(null, ['Your team is now unsubscribed from the event']);
        } else {
            if(!$this->subService->isSubscribedTeam($event, $team)) {
                return $this->api->success(null, ['Your team is already subscribed...']);
            }
            return $this->api->error(null, ['Ooops, we´re sorry. We can not unsubscribe your team from this event.']);
        }
    }

    /**
     * Retrieve subscribed team data of user from event
     *
     * @param Request $request
     * @param Event $event
     * @return void
     */
    public function teamSubscribed(Request $request, Event $event) {
        $user = Auth::guard('api')->user();

        return $this->api->success( $this->subService->getSubscribedTeam($event, $user) );
    }

    public function ban(Request $request, MatchData $match) {
        if($match->id <= 0) {
            throw new ApiException("Can not perform ban. Invalid match data!");
        }

        return $this->api->success($this->eventService->banFromEvent($match), ['Users baned']);
    }

    public function unBan(Request $request, MatchData $match) {
        if($match->id <= 0) {
            throw new ApiException("Can not perform ban. Invalid match data!");
        }

        return $this->api->success($this->eventService->unBanFromEvent($match), ['Users Unbanned']);
    }


    public function subMeta(Request $request, Event $event) {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->subService->getSubscriptionMeta($event, $user));
    }
}
