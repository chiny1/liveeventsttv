<?php

namespace App\Http\Controllers;

use App\Helper\ApiResponse;
use App\Models\Event;
use App\Repositories\PermissionRepository;
use App\Services\PermissionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    protected $api;
    protected $permService;

    public function __construct(PermissionService $permService)
    {
        $this->api = new ApiResponse();
        $this->permService = $permService;
    }

    public function modifyEvent(Request $request, Event $event) {
        $user = Auth::guard('api')->user();

        if($this->permService->canModifyEvent($event, $user)) {
            return $this->api->success();
        } else {
            return $this->api->permissionDenied(['You don´t have permission to modify this event']);
        }
    }
}
