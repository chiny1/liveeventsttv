<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Helper\ApiResponse;
use App\Models\Event;
use App\Models\PointTemplate;
use App\Repositories\TemplateRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Ui\Presets\React;

class TemplateController extends Controller
{
    protected $templateRepo;
    protected $api;

    public function __construct(TemplateRepository $template)
    {
        $this->templateRepo = $template;
        $this->api = new ApiResponse();
    }

    public function index(Request $request) {
        $user = Auth::guard('api')->user();

        return $this->api->success($this->templateRepo->templatesAll($user));
    }

    public function show(Request $request, PointTemplate $pointTemplate ) {
        //load template full here and return

        if($pointTemplate->id <= 0) {
            return $this->api->error(null, ['No Template found wiht the given id']);
        }

        $data = $this->templateRepo->getTemplateFull($pointTemplate);

        return $this->api->success($data);
    }

    public function store(Request $request, Event $event) {
        $data = $request->all();
        
        $this->validateTemplate($data);

        $user = Auth::guard('api')->user();

        return $this->api->success($this->templateRepo->storeTemplate($data, $event, $user), "Template saved.");
    }

    public function update(Request $request, Event $event, PointTemplate $pointTemplate) {
        $data = $request->all();
        
        $validator = Validator::make($request->all(), [
            'template_name' => "string|min:3|max:80",
        ]);
        
        if($validator->fails()) {
            return $this->erorr($validator->errors()->all());
        }
        
        $user = Auth::guard('api')->user();

        return $this->api->success($this->templateRepo->updateTemplate($data, $event, $pointTemplate, $user), "Template updated.");
    }

    public function delete(Request $request, PointTemplate $pointTemplate) {
        
        $user = Auth::guard('api')->user();

        return $this->api->success($this->templateRepo->deleteTemplate($pointTemplate, $user), "Template removed.");
    }

    public function templateByEvent(Event $event) {
        if($event->id > 0) {
            return $this->api->success($this->templateRepo->getTemplateByEvent($event));
        } else {
            return $this->api->success([], ['no event id specified']);
        }
    }

    protected function validateTemplate($requestData) {
        $validator = Validator::make($requestData, [
            'template_name' => "string|min:3|max:80|unique:point_templates,name",
        ]);
        
        if($validator->fails()) {
            //return $this->api->error(null, );
            $message = "";
            $errors = $validator->errors()->all();

            foreach($errors as $error) {
                $message .=$error.". ";
            }

            throw new ApiException($message);
        }
    }
}
