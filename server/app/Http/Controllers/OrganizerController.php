<?php

namespace App\Http\Controllers;

use App\Helper\ApiResponse;
use App\Models\Event;
use App\Repositories\EventOrganizerRepository;
use App\Repositories\interfaces\EventOrganizerRepositoryInterface;
use App\Repositories\interfaces\EventRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrganizerController extends Controller
{
    private $orgaRepo;
    private $eventRepo;
    private $api;

    public function __construct(EventOrganizerRepositoryInterface $orga, EventRepositoryInterface $events)
    {
        $this->orgaRepo = $orga;
        $this->eventRepo = $events;
        $this->api = new ApiResponse();
    }

    /**
     * Return all events the authenticated user is registered as organizer
     *
     * @return void
     */
    public function events() {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->eventRepo->organizerEvents($user));
    }

    public function update(Request $request, Event $event) {
        $user = Auth::guard('api')->user();

        if(!$event->isOrganizer($user)) {
            return $this->api->denied(['Only event team members can modify the member list of this event']);
        }
        

        return $this->api->success($this->orgaRepo->updateOrganizers($event, $request->all(), $user), [__('Organization list updated')]);
    }

    public function organizers(Request $request, Event $event) {
        return $this->api->success($this->orgaRepo->organizers($event));
    }

}

