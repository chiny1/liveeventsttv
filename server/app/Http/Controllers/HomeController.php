<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/app');
    }

    /**
     * Is used to store access Key receive from login.
     * Page stores AccessKey via locaStorage and redirects to /home
     * @return void
     */
    public function storeAccessKeySPA(Request $request) {
        $accessToken = session()->get('accessToken');
        return view('auth.storeAccessKey', ['accessToken' => $accessToken]);
    }

    public function removeAccessKeySPA() {
        return view('auth.removeAccessKey');
    }
}
