<?php

namespace App\Http\Controllers;

use App\Helper\ApiResponse;
use App\Models\Event;
use App\Services\LeaderBoardService;
use Illuminate\Http\Request;

class LeaderBoardController extends Controller
{
    private $leaderBoardService;
    private $api;

    public function __construct(LeaderBoardService $stats)
    {
        $this->leaderBoardService = $stats;
        $this->api = new ApiResponse(); //TODO REFACTOR THIS INTO SINGLETON -> Service?
    }

    public function solo(Request $request, Event $event) {
        return $this->api->success( $this->leaderBoardService->getSoloLeaderBoardStats($event) );
    }

    public function team(Request $request, Event $event) {

        return $this->api->success( $this->leaderBoardService->getTeamLeaderBoardStats($event) );
    }

    public function event(Request $request, Event $event) {
        return $this->api->success( $this->leaderBoardService->getEventStats($event) );
    }

}
