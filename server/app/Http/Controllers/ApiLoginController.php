<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApiLoginController extends Controller
{
    protected UserRepository $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function register(Request $request)
    {
        //@TODO: password length to 12
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required|string|min:12|confirmed',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);

        $user = $this->userRepo->add($request);
        $token = $this->userRepo->generateToken($user)->accessToken;
        $response = ['data' => [
            'token' => $token,
            'user' => new UserResource($user)
        ]];

        return response($response, 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            //@TODO: uncomment this when ready!
            //'password' => 'required|string|min:12|confirmed', //confirmed = password_confirmation field req*
            'password' => 'required|string|confirmed',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $user = $this->userRepo->byEmail($request);

        if ($user) {
            if (Auth::attempt($request->only('email', 'password'))) {
                $token = $this->userRepo->generateToken($user)->accessToken;
                $response = [
                    'data' => [
                        'token' => $token,
                        'user' => new UserResource($user)
                    ]
                ];

                return response($response, 200);
            }

            return response([
                'password' => __('The provided password does not match our records')
            ], 422);
        } else {
            return response([
                'email' => __('The provided email does not match our records')
            ], 422);
        }
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        if(!$user) {
            return response([
                'user' => __('User could not be resolved with the given accessToken')
            ], 422);
        }

        $token = $this->userRepo->getAccessToken($user);
        if(!$token) {
            return response([
                'user' => __('User has no access token.')
            ], 422);
        }

        $this->userRepo->removeAccessTokens($user);

        $response = ['message' => __('You have been successfully logged out!')];

        return response($response, 200);
    }
}
