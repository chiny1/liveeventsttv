<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Helper\ApiResponse;
use App\Models\Event;
use App\Models\LiveTicker;
use App\Repositories\interfaces\LiveTickerRepositoryInterface;
use App\Services\LeaderBoardService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LiveTickerController extends Controller
{

    protected $api;
    protected $liveRepo;
    protected $user;
    protected $leaderBoardService;

    public function __construct(LiveTickerRepositoryInterface $live, LeaderBoardService $leaderBoardService)
    {
        $this->liveRepo = $live;
        $this->api = new ApiResponse();
        $this->user = Auth::guard('api')->user();
        $this->leaderBoardService = $leaderBoardService;
    }

    /**
     * Show live ticker (for streaming purposes only)
     *
     * @param Request $request
     */
    public function index(Request $request) {
        return view('liveticker.index');
    }

    /**
     * return all livetickers fpr user
     *
     * @param Request $request
     */
    public function livetickersAll(Request $request) {
        return $this->api->success($this->liveRepo->getAllByUser($this->user));
    }


    public function liveticker(Request $request, Event $event) {
        return $this->api->success($this->liveRepo->getByUserEvent($this->user, $event));
    }

    /**
     * Create a new live ticker
     *
     * @param Request $request
     * @param Event $event
     * @return void
     */
    public function store(Request $request, Event $event) {
        return $this->api->success($this->liveRepo->createLiveTicker($this->user, $event));
    }

    /**
     * Get stats | coming from live ticker
     * 
     * @param Request $request
     */
    public function stats(Request $request) {
        $headerToken = $request->header('Authorization');
        $token = explode(' ', $headerToken)[1];
        
        $event = $this->liveRepo->getEventByToken($this->user, $token);

        if(!$event) {
            throw new ApiException("Can not retrieve event data. Acces Token invalid");
        }

        $stats = $this->leaderBoardService->getEventStats($event);

        return $this->api->success($stats);
    }
    
    public function delete(Request $request, LiveTicker $ticker) {
        return $this->api->success($this->liveRepo->removeLiveTicker($ticker));
    }
}
