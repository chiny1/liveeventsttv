<?php

namespace App\Http\Controllers;

use App\Enums\PermissionsEnum;
use App\Helper\ApiResponse;
use App\Models\User;
use App\Repositories\EventRepository;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $userRepo;
    protected $eventRepo;
    protected $teamRepo;
    protected $api;


    public function __construct(
        UserRepository $user, 
        EventRepository $event,
        TeamRepository $team)
    {
        $this->api = new ApiResponse();
        $this->userRepo = $user;
        $this->eventRepo = $event;
        $this->teamRepo = $team;
    }

    public function index() {
        if(!$this->api->can(PermissionsEnum::USER_SHOW)) {
            return $this->api->denied();
        }

        return $this->api->success($this->userRepo->all());
    }

    /**
     * Get user by ID
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function show(User $user) {
        if(!$this->api->can(PermissionsEnum::USER_SHOW)) {
            return $this->api->denied();
        }

        return $this->api->success($user);
    }

    public function store(Request $request) {
        if(!$this->api->can(PermissionsEnum::USER_CREATE)) {
            return $this->api->denied();
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:60',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:12|confirmed', //confirmed = password_confirmation field req*
        ]);

        if ($validator->fails()) {
            return $this->api->error(null, $validator->errors()->all());
        }

        $messages = [];
        $user = $this->userRepo->create($request->all());
        if(!$user) {
            $messages = array_push($messages, __("Invalid input data"));
            return $this->api->error(null, $messages);
        } 

        if($request['email_verified_at']) {
            $this->userRepo->verifyEmail($user, $request['email_verified_at']);
        }
        
        $messages = array_push($messages,__('User created'));
        return $this->api->success(null, $messages);
    }

    /**
     * update user
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function update(Request $request, User $user) {
        if(!$this->api->can(PermissionsEnum::USER_UPDATE)) {
            return $this->api->denied();
        }

        $user = User::find($user->id);
        $isNewEmail = false;
        $data = $request->all();

        if(!$user) {
            throw new ModelNotFoundException();
        }

        if($user->email !== $request['email']) {
            $isNewEmail = true;
        }

        if($isNewEmail) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255|unique:users',
            ]);
            
            if($validator->fails()) {
                return $this->api->error(null, $validator->errors()->all());
            }
        } else {
            unset($data['email']);
        }

        if(isset($data['password'])) {
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:12|confirmed',
            ]);
    
            if($validator->fails()) {
                return $this->api->error(null, $validator->errors()->all());
            }

            $data['password'] = Hash::make($request['password']);
            unset($data['password_confirmation']);
        } else {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $this->userRepo->verifyEmail($user, $data['email_verified_at']);
        
        $newUser = $this->userRepo->updateById($user->id, $data);

        return $this->api->success($newUser, ['User updated']);
    }

    public function updateProfile(Request $request, User $user) {
        //@TODO: add permission check!

        $user = User::find($user->id);
        $data = $request->all();

        if(!$user) {
            throw new ModelNotFoundException();
        }

        $updated = $this->userRepo->updateById($user->id, $data);

        return $this->api->success($updated, ['User updated']);
    }

    public function delete(User $user) {
        if(!$this->api->can(PermissionsEnum::USER_DELETE)) {
            return $this->api->denied();
        }

        //@TODO: remove user from every table before try to delete, bc of fk´s
        $user->delete();
        return $this->api->delete(['User removed']);
    }

    public function profile() {
        //@TODO: add can see profile permission
        $auth = Auth::guard('api')->user();
        $this->userRepo->permissions($auth);
        
        return $this->api->success($auth);
    }

    public function search(Request $request) {
        //@TODO: add can search Permission!

        $searchTerm = $request['search_term'];

        $validator = Validator::make($request->all() ,[
            'search_term' => "string|min:3"
        ]);

        if($validator->fails()) {
            return $this->api->error(null, $validator->errors()->all());
        }

        return $this->api->success($this->userRepo->search($searchTerm));
    }

    /**
     * retrieve all events the user is subscribed to
     *
     * @param User $user
     * @return void
     */
    public function events() {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->eventRepo->userEvents($user));
    }

    /**
     * get all events the user is subscribed to and he can submit data to
     *
     */
    public function eventsFiltered() {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->eventRepo->userEventsFiltered($user));
    }

    /**
     * Retrieve all teams the user created | team_leader
     *
     * @return void
     */
    public function teams() {
        $user = Auth::guard('api')->user();
        return $this->api->success($this->teamRepo->userTeams($user));
    }

    /**
     * Retrieve all permissions for the user performing this request | guard 'api'
     *
     * @return void
     */
    public function permissions() {
        $auth = Auth::guard('api')->user();
        return $this->api->success($this->userRepo->permissions($auth));
    }

    protected function validatePassword(Request $request) {
        return $request->validateWithBag('password', [
            'password' => 'required|string|min:12|confirmed',
        ]);
    }

    protected function validateEmail(Request $request) {
        return $request->validateWithBag('email', [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
    }

}
