<?php

namespace App\Helper\Submissions\Blocks;

/**
 * Block that checks if sum reached max value
 */
class BlockLimitMax extends BlockBase implements IBlocks {
    public function processBlock(int $sumFromCalculationsBefore) {
        $this->sum = $sumFromCalculationsBefore > $this->operand ? $this->operand : $this->sum;
    }
}