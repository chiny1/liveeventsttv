<?php 

namespace App\Helper\Submissions\Blocks;


interface IBlocks {
    public function processBlock(int $fieldValue);
}