<?php

namespace App\Helper\Submissions\Blocks;

/**
 * Block that calculates negative sum of field
 */
class BlockOperationMinus extends BlockBase implements IBlocks {

    public function processBlock(int $fieldValue) {
        $this->sum = ($fieldValue * $this->operand) * -1;
    }
}