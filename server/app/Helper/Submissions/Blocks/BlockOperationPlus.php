<?php


namespace App\Helper\Submissions\Blocks;

/**
 * Block that calculates sum of field
 */
class BlockOperationPlus extends BlockBase implements IBlocks {
    public function processBlock(int $fieldValue) {
        $this->sum = $fieldValue * $this->operand;
    }
}