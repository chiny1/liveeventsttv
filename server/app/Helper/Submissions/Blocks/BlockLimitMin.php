<?php

namespace App\Helper\Submissions\Blocks;

/**
 * Block that checks if sum reached min value
 */
class BlockLimitMin extends BlockBase implements IBlocks {

    public function processBlock(int $sumFromCalculationsBefore) {
        $this->sum = $sumFromCalculationsBefore < $this->operand ? 0 : $this->sum;
    }
}