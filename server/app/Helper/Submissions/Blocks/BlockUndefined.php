<?php

namespace App\Helper\Submissions\Blocks;

class BlockUndefined extends BlockBase implements IBlocks {

    public function processBlock(int $fieldValue) : int {
        return 0;
    }
}