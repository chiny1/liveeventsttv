<?php

namespace App\Helper\Submissions\Blocks;

abstract class BlockBase implements IBlocks {

    public $operand = 0;
    public $operator = "";
    public $sum = 0;
    
}