<?php


namespace App\Helper\Submissions;

use App\Helper\Submissions\Blocks\BlockLimitMax;
use App\Helper\Submissions\Blocks\BlockLimitMin;
use App\Helper\Submissions\Blocks\BlockOperationMinus;
use App\Helper\Submissions\Blocks\BlockOperationPlus;
use App\Helper\Submissions\Blocks\BlockUndefined;
use App\Helper\Submissions\Blocks\IBlocks;

/**
 * Submission Helper Class
 * 
 * Calculates Submissions by processing each FieldBlock (Block) of an exisisting 
 * PointTemplate (Field)
 */
class MatchHelper
{
    /**
     * Process blocks of an field
     *
     * @param [type] $field
     * @return int
     */
    public function getScore($field, $fieldValue)
    {
        $blocks = $field->blocks()->get();
        $sum = 0;

        foreach ($blocks as $block) {
            $_block = $this->getBlock($block);

            if($_block instanceof BlockUndefined) {
                continue;//skip unknown block!
            }

            if($_block instanceof BlockOperationPlus || $_block instanceof BlockOperationMinus) {
                $_block->processBlock($fieldValue);
                $sum += $_block->sum;
            }

            if($_block instanceof BlockLimitMax || $_block instanceof BlockLimitMin) {
                $_block->processBlock( $sum );
                $sum = $_block->sum;
            }
        }

        return $sum;
    }


    /**
     * Get the block type
     *
     * @param mixed $block
     * @return IBlocks
     */
    protected function getBlock($block): IBlocks
    {
        $_block = new BlockUndefined(); //refactor this to baseBlock!

        $blockName = $block->component_name;

        switch ($blockName) {

            case "BlockOperationPlusComponent":
                $_block = new BlockOperationPlus();
                break;
            case "BlockOperationMinusComponent":
                $_block = new BlockOperationMinus();
                break;
            case "BlockLimitMaxComponent":
                $_block = new BlockLimitMax();
                break;
            case "BlockLimitMinComponent":
                $_block = new BlockLimitMin();
                break;
            default:
                break;
        }

        //init block
        $blockData = json_decode($block->data);
        $_block->operand = $blockData->data->operand;
        $_block->operator = $blockData->data->operator;

        return $_block;
    }
}
