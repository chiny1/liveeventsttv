<?php


namespace App\Helper;

use App\Helper\ApiBaseResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Simple response helper for API calls
 */
class ApiResponse implements ApiBaseResponse {

    const HTTP_OK = "200";
    const HTTP_DELETE = "204";
    const HTTP_UNAUTHENTICATED = "401";
    const HTTP_DENIED = "403";
    const HTTP_ERROR = "422";
    const HTTP_PERMISSION_DENIED = "423";

    const responseStruct = [
        'data' => [],
        'message' => [],
    ];

    /**
     * Undocumented function
     *
     * @param [type] $data | Response Data
     * @param [type] $messages | Response messages
     * @param [type] $statusCode | HTTP status code
     * @return void
     */
    public function send($data, $messages = [], $statusCode = ApiResponse::HTTP_OK) {
        $this->responseStruct['data'] = $data;
        $this->responseStruct['message'] = $messages;
        $code = $statusCode;

        return response()->json($this->responseStruct, $code);
    }

    /**
     * Respond with data
     *
     * @param [type] $data
     * @param array $messages
     * @return void
     */
    public function success($data = [], $messages = [])
    {
        return $this->send($data, $messages);
    }

    /**
     * Respons with errors
     *
     * @param [type] $data
     * @param array $messages
     * @return void
     */
    public function error($data, $messages = [])
    {
        return $this->send($data, $messages, ApiResponse::HTTP_ERROR);
    }

    /**
     * Respons for deletes
     *
     * @param array $messages
     * @return void
     */
    public function delete($messages = []) {
        return $this->send(null, $messages, ApiResponse::HTTP_OK);
    }

    /**
     * Response for denied access
     *
     * @param array $messages
     * @return void
     */
    public function denied($messages = []) {
        return $this->send(null, $messages, ApiResponse::HTTP_DENIED);
    }


    /**
     * Permission check for currently authenticated user | guard 'api'
     *
     * @param [type] $permissionString
     * @return boolean
     */
    public function can($permissionString) {
        return Auth::guard('api')->user()->can($permissionString);
    }

    public function permissionDenied($message) {
        return $this->send(null, $message, ApiResponse::HTTP_PERMISSION_DENIED);
    }
}
