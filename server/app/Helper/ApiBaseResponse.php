<?php


namespace App\Helper;


interface ApiBaseResponse {
    function error($data, $messages = []);
    function success($data, $messages = []);
}
