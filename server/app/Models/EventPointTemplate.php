<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPointTemplate extends Model
{
    use HasFactory;

    protected $fillable = [
        "event_id",
        "point_template_id",
        "user_id"
    ];

    //which guard to use for spatie permissions
    protected $guard_name = "api";


    public function event() {
      return $this->hasOne(Event::class, 'event_id', 'id'); // todo check this... probably wrong syntax
    }

    public function template() {
        return $this->hasMany(PointTemplate::class, 'point_template_id', 'id');
    }

    public function getTemplateAssoc() {
        return PointTemplate::where('id', '=', $this->getAttributeValue('point_template_id'))->first();
    }
}
