<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointField extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "point_template_id"
    ];

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    public function blocks() {
        return $this->hasMany(FieldBlock::class);
    }
}
