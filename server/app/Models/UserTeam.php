<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserTeam extends Model
{
    use HasFactory, HasRoles;

    protected $guard_name = "api";

    public function team() {
        return $this->hasOne(Team::class, 'id', 'team_id');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
