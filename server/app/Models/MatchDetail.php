<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchDetail extends Model
{
    use HasFactory;

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    protected $fillable = [
        'field_value',
        'score',
        'match_id',
        'point_field_id',
    ];

    public function field() {
        return $this->hasOne(PointField::class, 'id', 'point_field_id');
    }
}
