<?php

namespace App\Models;

use App\Enums\VisibilityEnum;
use App\Repositories\EventOrganizerRepository;
use App\Repositories\EventRepository;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Event extends Model
{
    use  Notifiable, HasFactory, HasRoles;

    protected $fillable = [
        'title',
        'event_description',
        'start_date',
        'end_date',
        'event_mode',
        'vis_id',
    ];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];

     //which guard to use for spatie permissions
     protected $guard_name = "api";


     /**
      * get the event_mode associated with the event
      */
    public function event_mode() {
        return $this->belongsTo(EventMode::class, 'event_mode', 'id');
    }

    /**
     * get the event_visibility associated with the event
     */
    public function visibility() {
        return $this->belongsTo(EventVisibility::class, 'vis_id', 'id');
    }

    public function owner() {
        return $this->hasOne(User::class, 'id', 'owner');
    }

    protected $appends = [
        'participants',
        'organizers',
        'teams',
    ];

    public function getVisibilityAttribute() {
        return EventVisibility::where('id', '=', $this->vis_id)->first(); 
    }
    
    public function getParticipantsAttribute() {
        $repo = new EventRepository();
        return $repo->getParticipantNumber($this->getKey());
    }

    public function getOrganizersAttribute() {
        $repo = new EventOrganizerRepository();
        $orgas = $repo->organizersById($this->getKey());

        $users = [];
        foreach($orgas as $orga) {
            array_push($users, $orga->user);
        }

        return $users;
    }

    public function getTeamsAttribute() {
        return EventTeam::where('event_id', '=', $this->getKey())->count();
    }

    /**
     * Is the user provided an organizer of this event?
     *
     * @param User $user
     * @return boolean
     */
    public function isOrganizer(User $user) {
       if($user->hasRole('admin')) {
           return true;
       }

       return EventOrganizer::where([ ['event_id', '=', $this->getKey()], ['user_id', '=', $user->id]])->count() > 0;
    }

    /**
     * Is the user provided the owner of this event?
     *
     * @param User $user
     * @return boolean
     */
    public function isOwner(User $user) {
        $owner = $this->owner()->get();
        if(!$owner || !isset($owner[0])) {
            //no owner assigned. So user can not be owner of this event.
            return false;
        }

        if( $owner[0]->id == $user->id) {
            return true;
        }

        return false;
    }

    /**
     * Is this Event private?
     *
     * @return boolean
     */
    public function isPrivate() {
        $vis = $this->getVisibilityAttribute();
        return $vis->visibility == VisibilityEnum::PRIVATE;
    }

    /**
     * Does this event have subscribers?
     *
     * @return boolean
     */
    public function hasSubscriptions() {
        return EventUser::where('event_id', '=', $this->getKey())->count() > 0 || EventTeam::where('event_id', '=', $this->getKey())->count() > 0;
    }

    public function template() {
        return $this->belongsTo(PointTemplate::class, 'point_template_id');
    }
}
