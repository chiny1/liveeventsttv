<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class EventUser extends Model
{
    use HasFactory, HasRoles;

    //which guard to use for spatie permissions
    protected $guard_name = "api";


    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function event() {
        return $this->belongsTo(Event::class, 'event_id', 'id')->with('event_mode', 'visibility');
    }

    protected $fillable = [
        'user_id',
        'event_id',
    ];

    protected $appends = [
        'participants'
    ];
    
    public function getParticipantsAttribute() {
        return (isset($this->attributes['participants'])) ? $this->attributes['participants'] : [];
    }

}
