<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class EventMode extends Model
{
    use Notifiable, HasFactory, HasRoles;

    protected $fillable = [
        'name',
        'description',
    ];

     //which guard to use for spatie permissions
     protected $guard_name = "api";

     public function events() {
         return $this->hasMany(Event::class);
     }
}
