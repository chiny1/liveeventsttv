<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchData extends Model
{
    use HasFactory;

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    protected $fillable = [
        'image',
        'notes',
        'total_score',
        'approved',
        'approved_by',
        'event_id',
    ];

    public function details() {
        return $this->hasMany(MatchDetail::class, 'match_id')->with('field');
    }

    public function event() {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function matchable() {
        return $this->morphTo();
    }
}
