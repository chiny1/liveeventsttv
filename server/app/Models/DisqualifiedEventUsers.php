<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisqualifiedEventUsers extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'event_id',
    ];

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    public function user() {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function event() {
        return $this->belongsTo(Event::class, 'id', 'event_id');
    }
}
