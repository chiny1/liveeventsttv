<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Permission\Traits\HasRoles;

class Team extends Model
{
    use HasFactory, HasRoles;

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    protected $fillable = [
        'name',
        'team_leader'
    ];

    public function members() {
        return $this->hasMany(UserTeam::class, 'team_id')->with('user');
    }

    public function team() {
        return $this->hasOne(Team::class, 'team_id');
    }

    public function user() {
        return $this->hasOne(User::class, 'user_id');
    }

    public function getTeamAttribute() {
        return $this->team()->get();
    }

    public function getUserAttribute() {
        return $this->user()->get();
    }
   
    public function isLeader(User $user) {
        return $this->getAttributeValue('team_leader') === $user->id;
    }

    public function hasSubscription() {
        return EventTeam::where('team_id', '=', $this->getKey())->count();
    }

}
