<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamMatch extends Model
{
    use HasFactory;

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    protected $fillable = [
        'team_id',
        'match_data_id',
        'score',
        'event_id'
    ];

    public function team() {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }

    public function match() {
        return $this->belongsTo(MatchData::class, 'match_data_id', 'id');
    }

    public function event() {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }
}
