<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldBlock extends Model
{
    use HasFactory;

    protected $fillable = [
        "component_name",
        "data",
        "point_field_id"
    ];

    //which guard to use for spatie permissions
    protected $guard_name = "api";
}
