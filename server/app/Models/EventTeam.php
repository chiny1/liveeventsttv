<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class EventTeam extends Model
{
    use HasFactory, HasRoles;

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    
    public function event() {
        return $this->belongsTo(Event::class, 'event_id', 'id')->with('event_mode', 'visibility');
    }

    public function eventSimple() {
        return $this->belongsTo(Event::class, 'event_id', 'id')->get();
    }

    public function team() {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }

    protected $fillable = [
        'team_id',
        'event_id',
    ];

}
