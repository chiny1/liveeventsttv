<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Passport;

class LiveTicker extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_id',
        'user_id',
        'token',
        'token_id'
    ];

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function event() {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }
}
