<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointTemplate extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
    ];

    //which guard to use for spatie permissions
    protected $guard_name = "api";

    public function fields() {
      return $this->hasMany(PointField::class);
    }
}
