<?php 

namespace App\Services\interfaces;

use App\Models\Event;
use Illuminate\Database\Eloquent\Collection;

interface LeaderBoardServiceInterface {
    public function getSoloLeaderBoardStats(Event $event) : array;
    public function getTeamLeaderBoardStats(Event $event) : array;
    public function getEventStats(Event $event) : array;
}