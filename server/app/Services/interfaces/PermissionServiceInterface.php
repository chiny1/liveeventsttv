<?php 

namespace App\Services\interfaces;

use App\Models\Event;
use App\Models\MatchData;
use App\Models\User;

interface PermissionServiceInterface {
    public function canModifyEvent(Event $event, User $user) : bool;
    public function canBanUnbanUser(User $user, Event $event) : bool;
    public function isTeamLeader(User $user, MatchData $match) : bool;
    public function validateEventPermissions(Event $event, User $user, $data);
}