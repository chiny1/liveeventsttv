<?php 

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\Event;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PermissionService {

    /**
     * Can user modify this event?
     *
     * @param Event $event
     * @param User $user
     * @return boolean
     */
    public function canModifyEvent(Event $event, User $user) : bool {
        if($user->hasRole('admin')) {
            return true;
        }

        if($event->isOwner($user)) {
            return true;
        }

        if($event->isOrganizer($user)) {
            return true;
        }

        return false;
    }

    /**
     * Can user ban/unban other users from an event?
     *
     * @param User $user
     * @param Event $event
     * @return bool
     */
    public function canBanUnbanUser(User $user, Event $event) : bool {
        $callingUser = Auth::guard('api')->user();

        if($this->canModifyEvent($event, $callingUser)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if user can change event data
     *
     * @param Event $event
     * @param User $user
     * @param Array $data
     * @return void
     */
    public function validateEventPermissions(Event $event, User $user, $data)
    {
        //isOwner?
        if (!$event->isOwner($user)) {
            throw new ApiException("You are not the owner of this event. Modify denied!");
        }

        //can change event Mode?
        if (!$this->validateChangeEventMode($event, $data)) {
            throw new ApiException("Event Mode can´t be modified. Event is not private and has subscribers");
        }

    }

    /**
     * Can Event Mode be modified?
     *
     * @param Event $event
     * @return boolean
     */
    private function validateChangeEventMode(Event $event, $data)
    {
        if ($data['event_mode'] === $event->event_mode) {
            return true;
        }

        if (!$event->hasSubscriptions()) {
            return true;
        }

        if ($event->isPrivate()) {
            return true;
        }

        return false;
    }


}