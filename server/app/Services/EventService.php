<?php

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\Event;
use App\Models\MatchData;
use App\Models\User;
use App\Models\UserMatch;
use App\Repositories\interfaces\BanRepositoryInterface;
use App\Repositories\interfaces\EventRepositoryInterface;
use App\Traits\EventQueryFilter;
use Illuminate\Support\Arr;

class EventService {

    private $banRepo;
    private $eventRepo;
    private $permissions;

    public function __construct(BanRepositoryInterface $banRepo, EventRepositoryInterface $eventRepo, PermissionService $perms)
    {
        $this->banRepo = $banRepo;
        $this->eventRepo = $eventRepo;
        $this->permissions = $perms;
    }

    use EventQueryFilter;

   /**
     * Ban all subscribers of this match from the event
     *
     * @param MatchData $match
     */
    public function banFromEvent(MatchData $match) {
        $user = null;
        $event = null;

        $matchType = $match->matchable()->get()->first();
        $event = $matchType->event()->get()->first();

        if($matchType instanceof UserMatch) {
            $user = $matchType->user()->get()->first();

            $this->validateUserBanStatus($user, $event);

            return $this->banRepo->addUserToBanList($user, $event);
        } else {
            $team = $matchType->team()->get()->first();
            $members = $team->members()->get();

            foreach($members as $member) {
                $user = $member->user()->get()->first();

                $this->validateUserBanStatus($user, $event);

                $this->banRepo->addUserToBanList($user, $event);
            }
        }
    }

    protected function validateUserBanStatus(User $user, Event $event) {
        if(!$this->permissions->canBanUnbanUser($user, $event)) {
            throw new ApiException("Can not remove disqualify for user. Permission denied!");
        }
    }

    /**
     * Remove ban for all subscribers from the event
     *
     * @param MatchData $match
     */
    public function unBanFromEvent(MatchData $match) {
        $user = null;
        $event = null;
        
        $matchType = $match->matchable()->get()->first();
        $event = $matchType->event()->get()->first();

        if($matchType instanceof UserMatch) {
            $user = $matchType->user()->get()->first();

            $this->validateUserBanStatus($user, $event);

            return $this->banRepo->removeUserFromBanList($user, $event);
        } else {
            $team = $matchType->team()->get();
            $members = $team->members()->get();

            
            foreach($members as $member) {
                $user = $member->user()->get();
                
                $this->validateUserBanStatus($user, $event);
                
                $this->banRepo->removeUserFromBanList($user, $event);
            }
        }
       
    }

    public function eventPreviewRandom($num = 3)
    {
        if ($num > 10) {
            $num = 10;
        }

        if ($num < 3) {
            $num = 3;
        }

        //get random, distinct events with owner
        $eventQuery = Event::inRandomOrder()
            ->limit($num)
            ->distinct()
            ->where('owner', '!=', null);

        //only return public events
        $events = $this->addPublicQueryFilter($eventQuery)->get();

        $res = [];
        foreach ($events as $event) {
            $data = [
                'event' => null,
                'user' => null,
                'id' => 0,
            ];

            $_event = $this->eventRepo->singleEventFullId($event->id);
            $data['event'] = $_event;
            $data['user'] = Arr::first($event->owner()->get());
            $data['id'] = $event->id;
            array_push($res, $data);
        }

        return $res;
    }

    /**
     * Search for events with Title
     *
     * @param [type] $term
     * @return void
     */
    public function search($search)
    {
        return Event::where('title', 'like', "%{$search}%")
            ->with('event_mode')
            ->orderBy('title', 'ASC')
            ->get();
    }

}