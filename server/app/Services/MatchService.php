<?php

namespace App\Services;

use App\Enums\EventModeEnum;
use App\Events\Stats;
use App\Events\StatsChangedEvent;
use App\Exceptions\ApiException;
use App\Helper\Submissions\MatchHelper;
use App\Models\Event;
use App\Models\MatchData;
use App\Models\MatchDetail;
use App\Models\PointField;
use App\Models\TeamMatch;
use App\Models\User;
use App\Models\UserMatch;
use App\Repositories\interfaces\BanRepositoryInterface;
use App\Repositories\interfaces\EventRepositoryInterface;
use App\Repositories\interfaces\TeamRepositoryInterface;
use App\Services\PermissionService;
use App\Services\SubscriptionService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * Class SubmissionRepository.
 */
class MatchService
{
    protected $matchHelper;
    protected $eventRepo;
    protected $permissions;
    protected $teamRepo;
    protected $ban;
    protected $subService;

    public function __construct(
            EventRepositoryInterface $eventRepository, 
            PermissionService $perms,
            TeamRepositoryInterface $teams,
            BanRepositoryInterface $bans,
            SubscriptionService $subs
            )
    {
        $this->matchHelper = new MatchHelper();
        $this->eventRepo = $eventRepository;
        $this->permissions = $perms;
        $this->teamRepo = $teams;
        $this->banRepo = $bans;
        $this->subService = $subs;
    }

    public function addMatch(Event $event, $data, User $user) {
        DB::beginTransaction();

        //check ban Status
        if($this->banRepo->isUserBanned($user, $event)) {
            throw new ApiException("Can not add match data. You are banned from the event");
        }

        //has event stated
        if(Carbon::now() < Carbon::parse($event->start_date)) {
            throw new ApiException("Event hasn´t started yet");
        }

        //has event ended
        if(Carbon::now() > Carbon::parse($event->end_date)) {
            throw new ApiException("Event has ended");
        }
        
        $fields = $data['fields'];
        $matchData = $data['match'];

        $match = new MatchData();
        $match->image = $matchData['image'];
        $match->notes = $matchData['note'];
        $match->event_id = $event->id;
        $match->save();
        
        $totalScore = $this->addMatchDetails($fields, $match);
        $match->total_score = $totalScore;
        
        $eventMode = $event->event_mode()->first();
        $matchType = new stdClass();
        
        if($eventMode->name == EventModeEnum::TEAM) {
            $matchType = $this->addTeamMatch($event, $match, $user, $totalScore);
        } else if($eventMode->name == EventModeEnum::SOLO) {
            $matchType = $this->addUserMatch($event, $match, $user, $totalScore);
        } else if($eventMode->name == EventModeEnum::BOTH) {
            if($this->subService->isSubscribedAsTeam($event, $user)) {
                $matchType = $this->addTeamMatch($event, $match, $user, $totalScore);
            } else {
                $matchType = $this->addUserMatch($event, $match, $user, $totalScore);
            }
        }

        //save sub type to match
        $match->matchable_id = $matchType->id;
        $match->matchable_type = ($matchType instanceof UserMatch) ? UserMatch::class : TeamMatch::class;
        $match->save();

        DB::commit();

        // //broadcast stats updated
        // StatsChangedEvent::dispatch($event);

        return $match;
    }

    /**
     * Add match details and calculate total score for this match
     *
     * @param mixed $fields
     * @param MatchData $match
     * @return int
     */
    protected function addMatchDetails($fields, MatchData $match) : int {
        $totalScore = 0;

        foreach($fields as $field) {
            $pointFieldId = intval( $field['name'] );
            $pointField = PointField::find( $pointFieldId );

            if($pointField) {
                $fieldValue = intval( $field['value'] );
                $calculatedScoreForField = $this->matchHelper->getScore( $pointField, $fieldValue );
                
                $totalScore += $calculatedScoreForField;

                $matchDetail = new MatchDetail();
                $matchDetail->field_value = $fieldValue;
                $matchDetail->score = $calculatedScoreForField;
                $matchDetail->point_field_id = $pointFieldId;
                $matchDetail->match_id = $match->id;

                $matchDetail->save();
            }
        }

        return $totalScore;
    }

    public function recent(User $user) {
        $matches = $this->userMatchesAll($user);
        if($matches) {
            return $matches[count($matches)-1];
        } else {
            return [];
        }
    }

    public function matchFull(MatchData $match) {
        $subscribers = [];
        $teamName = "";
        $isTeam = false;
        $team = null;

        $matchAble = $match->matchable;
        if($matchAble instanceof UserMatch) {
            array_push($subscribers, $matchAble->user()->get()->first());
        } else {
            $teamFull = ($matchAble) ? $this->teamRepo->teamFull( $matchAble->team()->first() ) : null;
            $team = $teamFull['team'];
            if($teamFull) {
                $teamName = $team['name'];
                $isTeam = true;
                $members = $team['members'];

                foreach($members as $member) {
                    array_push($subscribers, $member->user()->get());
                }
            }

        }

        $response = [
            "match" => $match,
            "details" => $match->details()->get(),
            "event" => $match->event()->first(),
            "subscribers" => Arr::flatten( $subscribers ),
            "team_name" => $teamName,
            "isTeam" => $isTeam,
            "team" => $team ? $team : []
        ];

        return $response;
    }

    /**
     * Add Match to team_matches table
     *
     * @param Event $event
     * @param MatchData $match
     * @return MatchData
     */
    protected function addTeamMatch(Event $event, MatchData $match, User $user, $score) {
        
        $teamFullData = $this->subService->getSubscribedTeam($event, $user);
        $isTeamLeader = $teamFullData['team_leader'] == $user->id;

        if(!$teamFullData) {
            DB::rollback();
            throw new ApiException("Can not create match data. Team subscription not found.");
        }
        $subbedTeam = $teamFullData['team'];

        if(!$isTeamLeader) {
            // throw error
            DB::rollBack();
            throw new ApiException("Can not add match data. You are not the Team leader.");
        }

        $teamMatch = new TeamMatch();
        $teamMatch->team_id = $subbedTeam->id;
        $teamMatch->match_data_id = $match->id;
        $teamMatch->event_id = $event->id;
        $teamMatch->score = $score;
        $teamMatch->save();

        //add match data to every user in the team | so everybody can see their match results
        $members = $teamFullData['members'];
        foreach($members as $member) {
            $_user = $member->user()->first();
            $this->addUserMatch($event, $match, $_user, $score);
        }

        return $teamMatch;
    }

    /**
     * Add Match to user_matches table
     *
     * @param User $user
     * @param MatchData $match
     * @return void
     */
    protected function addUserMatch(Event $event, MatchData $match, User $user, $score) {
        $subbedUser = $this->subService->getSubscribedUser($event, $user);

        if(!$subbedUser) {
            DB::rollback();
            throw new ApiException("Can not create match data. User seems to be unsubscribed for the event.");
        }

        if(!$user || !$match) {
            DB::rollback();
            throw new ApiException("Can not create match data. User or Match data are missing.");
        }

        $userMatch = new UserMatch();
        $userMatch->user_id = $user->id;
        $userMatch->match_data_id = $match->id;
        $userMatch->event_id = $event->id;
        $userMatch->score = $score;
        $userMatch->save();

        return $userMatch;
    }

    public function allMatchesByEvent(User $user, Event $event) {
        $userMatches = UserMatch::where('event_id', '=', $event->id)->with('match')->orderBy('created_at', 'ASC')->get();
        $matches = $this->getFullMatchArray($userMatches);
        
        return $matches;
    }

    public function userMatchesByEvent(User $user, Event $event) {
        $userMatches = UserMatch::where([['user_id', '=', $user->id], ['event_id', '=', $event->id]])->with('match')->orderBy('created_at', 'ASC')->get();
        $matches = $this->getFullMatchArray($userMatches);
        
        return $matches;
    }
    
    public function userMatchesAll(User $user) {
        $userMatches = UserMatch::where('user_id', '=', $user->id)->with('match')->orderBy('created_at', 'ASC')->get();
        $matches = $this->getFullMatchArray($userMatches);
        
        return $matches;
    }

    /**
     * Is User leader of the team?
     *
     * @param User $user
     * @param MatchData $match
     * @return bool
     */
    public function isTeamLeader(User $user, MatchData $match) : bool {
        $fullMatch = $this->matchFull($match);
        $team = $fullMatch['team'];

        if($team) {
            return $team->isLeader($user);
        } else {
            return false;
        }
    }

    protected function getFullMatchArray(Collection $matches) {
        $response = [];
        foreach($matches as $userMatch) {
            array_push($response, $this->matchFull($userMatch->match));
        }

        return $response;
    }

    public function removeMatch(User $user, MatchData $match) {
        if($user->id > 0) {


            $details = $match->details()->get();
            $matchType = $match->matchable()->get()->first();

            if($matchType instanceof TeamMatch) {
                if(!$this->isTeamLeader($user, $match)) {
                    throw new ApiException("Can not remove match data. You are not the team leader!");
                }
            }

            $userMatches = UserMatch::where('match_data_id', '=', $match->id)->get();

            foreach($userMatches as $userMatch) {
                $userMatch->delete();
            }

            if($matchType && !$matchType->delete()) {
                throw new ApiException("Can not remove match data. Invalid match type");
            }
            
            foreach($details as $matchDetail) {
                $matchDetail->delete();
            }

            if($match->delete()) {
                return true;
            } else {
              return false;   
            }
        } else {
            throw new ApiException("Can not remove match data. Invalid user");
        }
    }
}
