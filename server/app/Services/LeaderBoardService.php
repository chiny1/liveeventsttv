<?php

namespace App\Services;

use App\Enums\EventModeEnum;
use App\Models\Event;
use App\Services\interfaces\LeaderBoardServiceInterface as InterfacesLeaderBoardServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\DB;

class LeaderBoardService implements InterfacesLeaderBoardServiceInterface {

    /**
     * Retrieve events solo (Player) ranking
     *
     * @param Event $event
     * @return array
     */
    public function getSoloLeaderBoardStats(Event $event) : array {
        $matches = [];

        if($this->isEventModeSolo($event) || $this->isEventModeTeam($event)) {
            $matches = $this->getSoloRanking($event);
        }

        return $matches;
    }


    /**
     * Retrieve events team ranking
     *
     * @param Event $event
     * @return array
     */
    public function getTeamLeaderBoardStats(Event $event) : array {
        $matches = [];

        if($this->isEventModeTeam($event) || $this->isEventModeBoth($event)) {
            $matches = $this->getTeamRanking($event);
        }

        return $matches;
    }


    /**
     * Retrieve all events stats
     *
     * @param Event $event
     * @return array
     */
    public function getEventStats(Event $event) : array {
        $res = [
            "teams" => [],
            "users" => []
        ];

        $res['teams'] = $this->getTeamLeaderBoardStats($event);
        $res['users'] = $this->getSoloLeaderBoardStats($event);

        return $res;
    }

    /**
     * Is EventMode Team?
     *
     * @param Event $event
     * @return boolean
     */
    private function isEventModeTeam(Event $event) {
        $event->event_mode()->get();
        if($event->name = EventModeEnum::TEAM) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * * Is EventMode Solo?
     *
     * @param Event $event
     * @return boolean
     */
    private function isEventModeSolo(Event $event) {
        $event->event_mode()->get();
        if($event->name = EventModeEnum::SOLO) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * * Is EventMode Both?
     *
     * @param Event $event
     * @return boolean
     */
    private function isEventModeBoth(Event $event) {
        $event->event_mode()->get();
        if($event->name = EventModeEnum::BOTH) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get only solo ranking stats
     *
     * @param Event $event
     */
    protected function getSoloRanking(Event $event) {
        return DB::select(DB::raw("SELECT events.id, user_matches.user_id, users.name, SUM(user_matches.score) AS score
            FROM user_matches
            JOIN users ON users.id = user_matches.user_id
            JOIN events ON events.id = user_matches.event_id
            WHERE events.id = ".$event->id."
            GROUP BY events.id, user_matches.user_id, users.name
            ORDER BY score DESC"));
    }

    /**
     * Get only team ranking stats
     *
     * @param Event $event
     */
    protected function getTeamRanking(Event $event) {
        return DB::select(DB::raw("SELECT events.id, team_matches.team_id, teams.name, SUM(team_matches.score) AS score
        FROM team_matches
        JOIN teams ON teams.id = team_matches.team_id
        JOIN events ON events.id = team_matches.event_id
        WHERE events.id = ".$event->id."
        GROUP BY events.id, team_matches.team_id, teams.name
        ORDER BY score DESC"));
    }
}