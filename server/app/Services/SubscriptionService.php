<?php 

namespace App\Services;

use App\Enums\SubscriptionStatusEnum;
use App\Exceptions\ApiException;
use App\Models\Event;
use App\Models\EventTeam;
use App\Models\EventUser;
use App\Models\Team;
use App\Models\User;
use App\Repositories\interfaces\TeamRepositoryInterface;
use Carbon\Carbon;

class SubscriptionService {

    private $teamRepo;

    public function __construct(TeamRepositoryInterface $teams)
    {
        $this->teamRepo = $teams;
    }

    /**
     * Subscribe single user to event
     *
     * @param Event $event
     * @param User $user
     * @return void
     */
    public function subscribe(Event $event, User $user)
    {
        //Is before start date?
        if ($event->start_date->lte(Carbon::now())) {
            throw new ApiException("Subscribing period for this event has ended on: " . Carbon::parse($event->start_date));
        }

        //Is event private?
        if ($event->isPrivate()) {
            throw new ApiException("Sorry, this event is not public. Subscription denied!");
        }

        //clear subscription first
        $this->unSubscribe($event, $user);

        $sub = new EventUser();
        $sub->event_id = $event->id;
        $sub->user_id = $user->id;
        return $sub->save();
    }

    /**
     * Unsubscribe single user from event
     *
     * @param Event $event
     * @param User $user
     * @return void
     */
    public function unSubscribe(Event $event, User $user)
    {
        return EventUser::where([['event_id', '=', $event->id], ['user_id', '=', $user->id]])->delete();
    }

    public function subscribeTeam(Event $event, Team $team, User $user)
    {
        //is user provided Teamleader?
        if (!$team->isLeader($user)) {
            throw new ApiException("You are not the team leader. Team subscription denied!");
        }

        //has team members that are already subscribed?
        $subbedMembers = $this->getSubscribedMembersByTeam($event, $team);
        if (count($subbedMembers) > 0) {
            $users = "";
            foreach ($subbedMembers as $user) {
                $users .= " " . $user->name . " ";
            }
            throw new ApiException("Unable to subscribe team. Users are already subscribed to event: " . $users);
        }

        //clear subscriptions first
        $this->removeTeamFromEvent($event, $team);

        $members = $team->members()->get();
        foreach ($members as $userTeam) {
            $user = $userTeam->user()->get()->first(); //maybe just first?
            $this->subscribe($event, $user);
        }

        // add Team to event_teams table
        $eventTeam = new EventTeam();
        $eventTeam->team_id = $team->id;
        $eventTeam->event_id = $event->id;
        return $eventTeam->save();
    }

    public function unSubscribeTeam(Event $event, Team $team, User $user)
    {
        //is user provided Teamleader?
        if (!$team->isLeader($user)) {
            throw new ApiException("You are not the team leader. Unsubscribe denied!");
        }

        $this->removeTeamFromEvent($event, $team);

        return true;
    }

    /**
     * Check subscription status of user and respond with status string
     *
     * @param Event $event
     * @param User $user
     * @return void
     */
    public function checkSubScriptionStatus(Event $event, User $user)
    {
        //check if User is Subscribed to event?

        if ($this->isSubscribed($event, $user)) {
            return SubscriptionStatusEnum::SUBSCRIBED;
        } else {
            return SubscriptionStatusEnum::UNSUBSCRIBED;
        }

        //blocked | ignore for now. Should be added later for blacklists for events!
    }

    /**
     * Get Subscription meta data for provided user and the given event
     *
     * @param Event $event
     * @param [type] $user
     */
    public function getSubscriptionMeta(Event $event, User $user) {
        $subStatus = $this->isSubscribed($event, $user);

        $responseMeta = [
            "status" => $subStatus,
            "subscriber" => null,
            "isTeam" => false,
        ];

        if($this->isSubscribedAsTeam($event, $user)) {
            $responseMeta['subscriber'] = $this->getSubscribedTeam($event, $user);
            $responseMeta['isTeam'] = true;
        } else {
            $responseMeta['subscriber'] = $this->getSubscribedUser($event, $user);
        }

        return $responseMeta;
    }

    public function isSubscribed(Event $event, User $user)
    {
        return EventUser::where([['event_id', '=', $event->id], ['user_id', '=', $user->id]])->sum('id') > 0;
    }

    public function isSubscribedTeam(Event $event, Team $team)
    {
        return EventTeam::where([['event_id', '=', $event->id], ['team_id', '=', $team->id]])->sum('id') > 0;
    }

    /**
     * Has user a team subscription for this event?
     *
     * @param Event $event
     * @param User $user
     * @return boolean
     */
    public function isSubscribedAsTeam(Event $event, User $user) {
        $subbedTeam = $this->getSubscribedTeam($event, $user);

        if($subbedTeam) {
           return true; 
        } else {
            return false;
        }
    }

    /**
     * Get team from user that is subscribed to the given event
     *
     * @param Event $event
     * @param User $user
     */
    public function getSubscribedTeam(Event $event, User $user)
    {
        $eventTeam = EventTeam::where('event_id', '=', $event->id)->whereHas('team', function ($teams) use ($user) {
            $teams->whereHas('members', function ($userTeam) use ($user) {
                $userTeam->where('user_id', '=', $user->id);
            });
        })->get()->first();

        if (!$eventTeam) {
            return null;
        }

        $teamDataSimple = $eventTeam->team()->get()->first();

        return $this->teamRepo->teamFull($teamDataSimple);;
    }

     /**
     * Get user that is subscibed to the given event
     */
    public function getSubscribedUser(Event $event, User $user)
    {
        $eventUser = EventUser::where([['event_id', '=', $event->id], ['user_id', '=', $user->id]])->first();
        if ($eventUser) {
            return $eventUser->user()->first();
        } else {
            null;
        }
    }


    /**
     * Get subscribed users of an event
     *
     * @param Event $event
     * @param Team $team
     * @return Array
     */
    private function getSubscribedMembersByTeam(Event $event, Team $team)
    {
        $members = $team->members()->get();

        $subscribedUsers = [];
        foreach ($members as $userTeam) {
            $user = $userTeam->user()->get()->first();

            if ($this->isSubscribed($event, $user)) {
                array_push($subscribedUsers, $user);
            }
        }

        return $subscribedUsers;
    }


    private function removeTeamFromEvent(Event $event, Team $team)
    {
        EventTeam::where([['event_id', '=', $event->id], ['team_id', '=', $team->id]])->delete();

        $members = $team->members()->get();
        foreach ($members as $eventTeam) {
            $user = $eventTeam->user()->get()->first();
            $this->unSubscribe($event, $user);
        }

        return;
    }

    /**
     * Remove all subscribers for this event.
     *
     * @param Event $event
     * @return void
     */
    public function removeSubscriptionsForEvent(Event $event)
    {
        EventUser::where('event_id', '=', $event->id)->delete();
        EventTeam::where('event_id', '=', $event->id)->delete();
    }

}