<?php

namespace App\Exceptions;

use App\Helper\ApiResponse;
use Exception;
use Throwable;

/**
 * Custom API Exception class that can be thrown, to repost API errors correctly
 */
class ApiException extends Exception {

    protected $message = "";

    public function __construct($message)
    {
        $this->message = $message;
        parent::__construct();
    }

    /**
     * Render an exception into an HTTP response.
     * 
     *  @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        $api = new ApiResponse();
        return $api->error(null, $this->message);
    }
}