<?php 

namespace App\Utils;

class ArrayUtils {

    public static function removeDuplicates($array) {
        return array_unique($array, SORT_REGULAR);
    }

    public static function removeDefaultUser($members, $user) {
        if(!$user) {
            return;
        }

        for($i = 0; $i < count($members); $i++) {
            $member = $members[$i];
            if(isset($member['user'])) {
                $member = $member['user'];
            }

            if($member['id'] == $user->id) {
                array_splice($members, $i, 1);
            }
        }
    }
}