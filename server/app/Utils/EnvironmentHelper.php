<?php


namespace App\Utils;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use League\CommonMark\Environment;

/**
 * Helper Class for Testin | Debuging | Local Development helpers
*/
class EnvironmentHelper {

    const ENV_TESTING = "testing";
    const ENV_DEVELOPEMENT = "local";
    const ENV_PRODUCTION = "production";

    public static function isDevelopement() : bool {
        if(App::environment() === EnvironmentHelper::ENV_DEVELOPEMENT) {
            return true;
        } else {
            return false;
        }
    }

    public static function isTesting() : bool {
        if(App::environment() === EnvironmentHelper::ENV_TESTING) {
            return true;
        } else {
            return false;
        }
    }

    public static function isProduction() : bool {
        if(App::environment() === EnvironmentHelper::ENV_PRODUCTION) {
            return true;
        } else {
            return false;
        }
    }

    public static function getEnvironmentMode() {
        return App::environment();
    }
}
