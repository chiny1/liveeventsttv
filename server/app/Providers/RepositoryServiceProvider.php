<?php

namespace App\Providers;

use App\Repositories\BanRepository;
use App\Repositories\EventOrganizerRepository;
use App\Repositories\EventRepository;
use App\Repositories\interfaces\BanRepositoryInterface;
use App\Repositories\interfaces\EventOrganizerRepositoryInterface;
use App\Repositories\interfaces\EventRepositoryInterface;
use App\Repositories\interfaces\LeaderBoardRepositoryInterface;
use App\Repositories\interfaces\LiveTickerRepositoryInterface;
use App\Repositories\interfaces\MatchRepositoryInterface;
use App\Repositories\interfaces\PermissionRepositoryInterface;
use App\Repositories\interfaces\TeamRepositoryInterface;
use App\Repositories\interfaces\TemplateRepositoryInterface;
use App\Repositories\interfaces\UserRepositoryInterface;
use App\Repositories\LeaderBoardRepository;
use App\Repositories\LiveTickerRepository;
use App\Repositories\MatchRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\TeamRepository;
use App\Repositories\TemplateRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind(BanRepositoryInterface::class, BanRepository::class);
       $this->app->bind(EventOrganizerRepositoryInterface::class, EventOrganizerRepository::class);
       $this->app->bind(EventRepositoryInterface::class, EventRepository::class);
       $this->app->bind(LeaderBoardRepositoryInterface::class, LeaderBoardRepository::class);
       $this->app->bind(MatchRepositoryInterface::class, MatchRepository::class);
       $this->app->bind(PermissionRepositoryInterface::class, PermissionRepository::class);
       $this->app->bind(TeamRepositoryInterface::class, TeamRepository::class);
       $this->app->bind(TemplateRepositoryInterface::class, TemplateRepository::class);
       $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
       $this->app->bind(LiveTickerRepositoryInterface::class, LiveTickerRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
