<?php

namespace App\Repositories;

use App\Exceptions\ApiException;
use App\Models\Event;
use App\Models\LiveTicker;
use App\Models\User;
use App\Repositories\interfaces\LiveTickerRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class LiveTickerRepository.
 */
class LiveTickerRepository extends BaseRepository implements LiveTickerRepositoryInterface
{
    const LIVETICKER_ACCES_KEY_NAME = "liveticker";

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return LiveTicker::class;
    }

    public function getAllByUser(User $user) : Collection {
        return LiveTicker::where('user_id', '=', $user->id)->with('event')->get();
    }

    public function getByUserEvent(User $user, Event $event) : LiveTicker {
        //maybe dont use first here, if we like to have multiple tickers for this event.... !!!
        return LiveTicker::where([['user_id', '=', $user->id], ['event_id', '=', $event->id]])->with('event', 'user')->get()->first();
    }

    public function createLiveTicker(User $user, Event $event) : LiveTicker {
        $liveTicker = new LiveTicker();

        if(!$event->isOwner($user )) {
            throw new ApiException("Can not create accesToken. You are not the owner of this event!");
        }

        //TODO: add event ID in here
        $personalAccessToken = $user->createToken(LiveTickerRepository::LIVETICKER_ACCES_KEY_NAME, ['check-stats']);

        $liveTicker->event_id = $event->id;
        $liveTicker->user_id = $user->id;
        $liveTicker->token = $personalAccessToken->accessToken;
        $liveTicker->token_id = $personalAccessToken->token->id;

        $liveTicker->save();

        return $liveTicker;
    }

    /**
     * Delete LiveTicker and the accoridng access token
     *
     * @param LiveTicker $ticker
     */
    public function removeLiveTicker(LiveTicker $ticker) {
        
        DB::table('oauth_access_tokens')->where('id', '=', $ticker->token_id)->delete();

        return $ticker->delete();
    }

    public function getEventByToken(User $user, $token) : Event {
        $liveTicker = LiveTicker::where([['user_id', '=', $user->id], ['token', '=', $token]])->first();

        if(!$liveTicker) {
            throw new ApiException("No event found for this access token");
        }

        return $liveTicker->event;
    }
}
