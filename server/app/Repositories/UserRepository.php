<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Laravel\Passport\PersonalAccessTokenResult;
use Laravel\Passport\Token;
use phpDocumentor\Reflection\Types\Boolean;

//use Your Model

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    protected const ACCESS_TOKEN_NAME = "authAccessToken_TTV";

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }

    public function byEmail(Request $request) : User {
        $email = $request->only('email');
        return $this->where('email', $email)->first();
    }

    public function add(Request $request) : User {
        $user = $this->create($request->all());
        $user->save();

        return $user;
    }

    /**
     * Generate Users personall Access Token
    */
    public function generateToken(User $user) : PersonalAccessTokenResult {
        return $user->createToken( UserRepository::ACCESS_TOKEN_NAME );
    }

    /**
     *Get Users Access Token
     *
     * @return Token|null
     */
    public function getAccessToken(User $user) {
        return $user->token();
    }

    /**
     * Revoke users accessToken
     */
    public function revokeAccessToken(User $user) : bool {
        $token = $this->getAccessToken($user);

        if($token) {
             return $token->revoke();
        } else {
            return false;
        }
    }

    /**
     * Remove users accessTokens
    */
    public function removeAccessTokens(User $user) : bool {
        $user->tokens()->each(function ($token, $key) {
            $token->delete();
        });

        return true;
    }


    /**
     * Get users matching search term
     *
     * @param String $term
     * @return void
     */
    public function search(string $term) {
        return User::where('name', 'like', '%'.$term.'%')
            ->orWhere('email', 'like', '%'.$term.'%')
            ->with('roles')->get();
    }

    public function verifyEmail(User $user, $verifiedAt) {
        $user->email_verified_at = $verifiedAt;
        $user->save();

        $user->sendEmailVerificationNotification();
    }

    public function permissions(User $user) {
        return $user->getAllPermissions();
    }
}
