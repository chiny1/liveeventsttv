<?php

namespace App\Repositories;

use App\Models\Event;
use App\Models\EventMode;
use App\Models\EventOrganizer;
use App\Models\EventTeam;
use App\Models\EventUser;
use App\Models\EventVisibility;
use App\Models\Team;
use App\Models\User;
use App\Repositories\interfaces\EventRepositoryInterface;
use App\Traits\EventQueryFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class EventRepository.
 */
class EventRepository extends BaseRepository implements EventRepositoryInterface
{

    use EventQueryFilter;

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Event::class;
    }

    public function teamEvents(Team $team) : Collection
    {
        return EventTeam::where('team_id', $team->id)->with('team', 'event')->get();
    }

    /**
     * Get all events the user is registered to 
     *
     * @param User $user
     * @return array
     */
    public function userEvents(User $user) : array
    {
        $events = $this->getEvents(EventUser::class, $user);

        $res = [];
        foreach ($events as $eventUser) {
            $event = $this->singleEventFullId($eventUser->event_id);
            if (isset($event[0])) {
                array_push($res, $event[0]);
            }
        }

        return $res;
    }


    /**
     * Get all events the user is subscribed to and pre filter events:
     * 
     * that havent started or that have already ended
     *
     * @param User $user
     * @return array
     */
    public function userEventsFiltered(User $user) : array
    {
        $events = $events = EventUser::with('user', 'event')->where('user_id', '=', $user->id)
        ->whereHas('event', function($event) {
            return $event->where([['start_date', '<', Carbon::now()], ['end_date', '>', Carbon::now()]]);
        })->get();

        $res = [];
        foreach ($events as $eventUser) {
            $event = $this->singleEventFullId($eventUser->event_id);
            if (isset($event[0])) {
                array_push($res, $event[0]);
            }
        }

        return $res;
    }

    /**
     * Get all events the user is registered as organizer  | (this means Hosting an event)
     *
     * @param User $user
     * @return array
     */
    public function organizerEvents(User $user) : array
    {
        $events = EventOrganizer::where('user_id', '=', $user->id)->get();

        $res = [];
        foreach ($events as $event) {
            array_push($res, $this->singleEventFullId($event->event_id));
        }

        return Arr::flatten($res);
    }

    /**
     * Get all events the user is owner
     *
     * @param User $user
     * @return Collection
     */
    public function ownerEvents(User $user) : Collection
    {
        return Event::where('owner', '=', $user->id)->with('event_mode', 'visibility')->get();
    }

    /**
     * Update corresponding event data
     *
     * @param Event $event
     * @param array $data
     * @param User $user
     * @return void
     */
    public function updateEvent(Event $event, $data)
    {
        return $this->updateById($event->id, $data);
    }

    /**
     * Retrieve all events with event modes realtion
     * 
     * @return Collection
     */
    public function allEvents() : Collection
    {
        $eventsQuery = Event::with(['event_mode', 'visibility']);
        return $this->addPublicQueryFilter($eventsQuery)->get();
    }

    /**
     * Retrieve private events only
     * 
     * @return Collection
     */
    public function allPrivateEvents() : Collection
    {
        $eventsQuery = Event::with(['event_mode', 'visibility']);
        return $this->addPrivateQueryFilter($eventsQuery)->get();
    }

    /**
     * Return number of participating users for this event
     * 
     * @return int
     */
    public function getParticipantNumber($event_id) : int
    {
        return EventUser::where('event_id', '=', $event_id)->count();
    }

    /**
     * Retrieve all event modes
     * 
     * @return Collection
     */
    public function eventModes() : Collection
    {
        return EventMode::all();
    }

    /**
     * Retrieve all possible event visibility options
     * 
     * @return Collection
     */
    public function visibilities() : Collection
    {
        return EventVisibility::all();
    }

    /**
     * Retrieve single Event:
     * 
     * resolves full relations of event and | returns all event_modes, event_visibilities
     * retrieves all event_modes, event_visibilities
     *
     * @param Event $event
     * @return array
     */
    public function singleEvent(Event $event) : array
    {
        $modes = $this->eventModes();
        $vis = $this->visibilities();

        return [
            'event_modes' => $modes,
            'event_visibilities' => $vis,
            'event' => $event,
        ];
    }

    /**
     * Retrieve single Event data with resolved relations:
     * 
     * resolves associated visibility
     * resolves associated event mode
     *
     * @param Event $event
     * 
     */
    public function singleEventFull(Event $event) : Collection
    {
        return $this->singleEventFullId($event->id);
    }

    /**
     * Retrieve single Event data with resolved relations: by Event-ID
     *
     * @param [type] $id
     * @return Collection
     */
    public function singleEventFullId($id) : Collection
    {
        $res = Event::with('event_mode', 'visibility', 'owner')->where('id', '=', $id)->get();
        return $res;
    }

    /**
     * Retrieve Domain Data for events
     *
     * @return array
     */
    public function domains() : array
    {
        $modes = $this->eventModes();
        $vis = $this->visibilities();

        return [
            'event_modes' => $modes,
            'event_visibilities' => $vis,
        ];
    }

    /**
     * Create new event
     *
     * @param mixed $data
     * @param User $owner
     * 
     * @return Event
     */
    public function store($data, User $owner) : Event
    {
        $event = $this->create($data);
        $event->owner = $owner->id;
        $event->save();

        return $event;
    }

    /**
     * Get events for specific models
     *
     * @param String $model
     * @param User $user
     */
    protected function getEvents(String $model, User $user)
    {
        $events = $model::with('user', 'event')
            ->where('user_id', '=', $user->id)->get();

        return $events;
    }

}
