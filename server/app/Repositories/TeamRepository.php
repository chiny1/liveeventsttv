<?php

namespace App\Repositories;

use App\Exceptions\ApiException;
use App\Models\EventTeam;
use App\Models\Team;
use App\Models\User;
use App\Models\UserTeam;
use App\Repositories\interfaces\TeamRepositoryInterface;
use Illuminate\Support\Arr;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class TeamRepository.
 */
class TeamRepository extends BaseRepository implements TeamRepositoryInterface
{

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Team::class;
    }

    /**
     * Create a Team with members and assign $user as team leader
     *
     * @param User $user
     * @param Array $data
     */
    public function createTeam(User $user, $data) : Team {
        $members = $data['team_members'];
        $teamName = $data['team_name'];

        $team = new Team();
        $team->name = $teamName;
        $this->updateTeamLeader($team, $user); //make requesting user team leader
        
        $this->addMember($team, $user); //add requesting user as first member
        
        $this->addMembers($team, $members, $user);

        return $team;
    }

    /**
     * Update team data
     *
     * @param User $user
     * @param Team $team
     * @param Array $data
     */
    public function updateTeam(User $user, Team $team, $data) : Team {
        $members = $data['team_members'];
        $teamName = $data['team_name'];
        
        $this->validatePermission($user, $team);

        $this->remMembers($team);

        $team->name = $teamName;

        $this->updateTeamLeader($team, $user); //make requesting user teamleader

        $this->addMember($team, $user);
        
        $this->addMembers($team, $members, $user);

        return $team;
    }

    protected function validatePermission(User $user, Team $team) {
        //is user team leader?
        if(!$team->isLeader($user)) {
            throw new ApiException("You are not the team leader. Modify team denied!");
        }

        //is team participating in events?
        $subCount = $team->hasSubscription();
        if($subCount > 0) {
            throw new ApiException("Team is subscribed to ".$subCount." events. Modify team denied!");
        }

        return true;
    }


    /**
     * Get all Teams the user is team leader
     *
     * @param User $user
     * @return array
     */
    public function userTeams(User $user) : array {
        $teams = Team::where('team_leader', '=', $user->id)->get();
        $response = [];

        foreach($teams as $team) {
            array_push($response, $this->teamFull($team));
        }

        return $response;
    }

    /**
     * Get more team info
     *
     * @param Team $team
     * @return array
     */
    public function teamFull(Team $team) : array {
        $members = $team->members()->get();
        $eventTeams = EventTeam::where('team_id', '=', $team->id)->get();
        $events = [];

        foreach($eventTeams as $et) {
            array_push($events, $et->eventSimple());
        }

        $response = [
            'id' => $team->id,
            'name' => $team->name,
            'team_leader' => $team->team_leader,
            'members' => $members,
            'events' => Arr::flatten( $events ),
            'team' => $team,
        ];

        return $response;
    }

    /**
     * Get all Teams the user is assigned to (not leader)
     *
     * @param User $user
     * @return array
     */
    public function teamsUser(User $user) : array {
        $teams = UserTeam::where('user_id', '=', $user->id)->with('team', 'user')->get();
        $response = [];

        foreach($teams as $entry) {
            $team = $entry->team()->get()[0];

            if($team->isLeader($user)) {
                continue;
            }

            array_push($response, $this->teamFull($team));
        }

        return $response;
    }


    /**
     * Retrieve Single Team data
     *
     * @param Team $team
     * @return array
     */
    public function team(Team $team) : array {
        $response = [
            'name' => $team->name,
            'team' => $team,
            'team_members' => $team->members()->get()
        ];

        return $response;
    }

    /**
     * Add members to a team
     *
     * @param Team $team
     * @param [type] $members
     * @param User $user
     * @return void
     */
    public function addMembers(Team $team, $members, User $user = null) {
        foreach($members as $member) {
            
            if(isset($member['user'])) {
                $member = $member['user'];
            }

            if($user) {
                if($member['id'] == $user->id) {
                    continue; //skip user provided
                }
            }
            $userTeam = new UserTeam();
            $userTeam->user_id = $member['id'];
            $userTeam->team_id = $team->id;
            $userTeam->save();
        }
    }

    public function addMember(Team $team, User $user) {
        $members = [];
        array_push($members, $user->toArray());
        $this->addMembers($team, $members);
    }

    /**
     * Delete members of a team
     *
     * @param Team $team
     * @return int
     */
    public function remMembers(Team $team) : int {
        return UserTeam::where('team_id', '=', $team->id)->delete();
    }

    public function remMember(User $user) {
        return UserTeam::where('user_id', '=', $user->id)->delete();
    }

    public function updateTeamLeader(Team $team, User $user) {
        $team->team_leader = $user->id; //assign user that made the request
        return $team->save();
    }

    public function deleteTeam(Team $team, User $user) {
        $this->validatePermission($user, $team);

        $this->remMembers($team);

        return $team->delete();
    } 
}
