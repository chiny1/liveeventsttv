<?php 

namespace App\Repositories\interfaces;

use App\Models\Event;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface EventRepositoryInterface {
    public function teamEvents(Team $team) : Collection;
    public function userEvents(User $user) : array;
    public function organizerEvents(User $user) : array;
    public function ownerEvents(User $user) : Collection;
    public function updateEvent(Event $event, $data);
    public function allEvents() : Collection;
    public function allPrivateEvents() : Collection;
    public function getParticipantNumber($event_id) : int;
    public function eventModes() : Collection;
    public function visibilities() : Collection;
    public function singleEvent(Event $event) : array;
    public function singleEventFull(Event $event) : Collection;
    public function singleEventFullId($id) : Collection;
    public function domains() : array;
    public function store($data, User $owner) : Event;
}