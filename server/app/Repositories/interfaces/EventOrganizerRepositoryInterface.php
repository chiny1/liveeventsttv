<?php 

namespace App\Repositories\interfaces;

use App\Models\Event;
use App\Models\EventOrganizer;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface EventOrganizerRepositoryInterface {
    public function add(User $user, Event $event) : EventOrganizer;
    public function addUsers(Collection $users, Event $event);
    public function remove(User $user);
    public function addMembers(Event $event, $members, User $user = null);
    public function remMembers(Event $event);
    public function updateOrganizers(Event $event, $members, User $user = null);
    public function organizers(Event $event);
    public function organizersById($eventId);
}