<?php 

namespace App\Repositories\interfaces;

use App\Models\Team;
use App\Models\User;

interface TeamRepositoryInterface {
    public function createTeam(User $user, $data) : Team;
    public function updateTeam(User $user, Team $team, $data) : Team;
    public function userTeams(User $user) : array;
    public function teamFull(Team $team) : array;
    public function teamsUser(User $user) : array;
    public function team(Team $team) : array;
    public function addMembers(Team $team, $members, User $user = null);
    public function addMember(Team $team, User $user);
    public function remMembers(Team $team);
    public function remMember(User $user);
    public function updateTeamLeader(Team $team, User $user);
    public function deleteTeam(Team $team, User $user);
}