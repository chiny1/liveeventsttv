<?php 

namespace App\Repositories\interfaces;

use App\Models\Event;
use App\Models\LiveTicker;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface LiveTickerRepositoryInterface {
    public function getAllByUser(User $user) : Collection;
    public function getByUserEvent(User $user, Event $event) : LiveTicker;
    public function createLiveTicker(User $user, Event $event) : LiveTicker;
    public function getEventByToken(User $user, $token) : Event;
    public function removeLiveTicker(LiveTicker $ticker);
}