<?php 

namespace App\Repositories\interfaces;

use App\Models\DisqualifiedEventUsers;
use App\Models\Event;
use App\Models\User;

interface BanRepositoryInterface {
    public function addUserToBanList(User $user, Event $event) : DisqualifiedEventUsers;
    public function isUserBanned(User $user, Event $event) : bool;
    public function removeUserFromBanList(User $user, Event $event) : bool;
}