<?php

namespace App\Repositories;

use App\Models\Event;
use App\Models\EventOrganizer;
use App\Models\EventUser;
use App\Models\User;
use App\Repositories\interfaces\EventOrganizerRepositoryInterface;
use App\Utils\ArrayUtils;
use Illuminate\Support\Collection;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class EventOrganizerRepository.
 */
class EventOrganizerRepository extends BaseRepository implements EventOrganizerRepositoryInterface
{

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return EventOrganizer::class;
    }

    /**
     * Add single user to event organizers
     *
     * @param User $user
     * @return EventOrganizer
     */
    public function add(User $user, Event $event) : EventOrganizer {
        $orga = new EventOrganizer();
        $orga->user_id = $user->id;
        $orga->event_id = $event->id;
        $orga->save();

        return $orga;
    }

    /**
     * Add user collection to event organizers
     *
     * @param Collection $users
     * @param Event $event
     * @return void
     */
    public function addUsers(Collection $users, Event $event) {
        foreach($users as $user) {
            $this->add($user, $event);
        }
    }

    /**
     * Remove organizer
     *
     * @param User $user
     * @return void
     */
    public function remove(User $user) {
        return EventOrganizer::where('user_id', '=', $user->id)->delete();
    }

    public function remMembers(Event $event) {
        return EventOrganizer::where('event_id', '=', $event->id)->delete();
    }
    
    public function addMembers(Event $event, $members, User $user = null) {
        $data = ArrayUtils::removeDuplicates($members); 
        
        foreach($data as $member) {
            if(isset($member['user'])) {
                $member = $member['user'];
            }

            $user = User::findOrFail($member['id']);
            $this->add($user, $event);
        }
    }

    /**
     * Update Organizer entries.
     *
     * @param Event $event
     * @param Array<Users> $members
     * @param User $user | default organizer for event
     * @return void
     */
    public function updateOrganizers(Event $event, $members, User $user = null) {
        $this->remMembers($event);
        $this->addMembers($event, $members['members'], $user);
    }

    public function organizers(Event $event) {
        return $this->organizersById($event->id);
    }

    public function organizersById($eventId) {
        return EventOrganizer::where('event_id', '=', $eventId)->with('user', 'roles')->get();
    }

}
