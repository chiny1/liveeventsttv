<?php

namespace App\Repositories;

use App\Exceptions\ApiException;
use App\Models\Event;
use App\Models\EventPointTemplate;
use App\Models\FieldBlock;
use App\Models\MatchDetail;
use App\Models\PointField;
use App\Models\PointTemplate;
use App\Models\User;
use App\Repositories\interfaces\TemplateRepositoryInterface;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use stdClass;

/**
 * Class TemplateRepository.
 */
class TemplateRepository extends BaseRepository implements TemplateRepositoryInterface
{


    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return PointTemplate::class;
    }

    /**
     * Get all templates the user owns
     *
     * @param User $user
     */
    public function templatesAll(User $user) {
        $eventTemplates = EventPointTemplate::where('user_id', '=', $user->id)->get();

        $templates = [];
        foreach($eventTemplates as $eventTemplate) {
            array_push($templates, $eventTemplate->getTemplateAssoc()); 
        }

        return $templates;
    }

    /**
     * Store a fresh template
     *
     * @param [type] $data
     * @param Event $event
     * @param User $user
     */
    public function storeTemplate($data, Event $event, User $user) {
        DB::beginTransaction();
       
        $this->checkUserId($user);

        $templateName = $data['template_name'];

        $template = $this->addTemplate($templateName);

        $this->addEventPointTemplate($event, $template, $user);

        $this->addFields($data, $template);

        $this->assignTemplateToEvent($event, $template);
        
        DB::commit();
        return $template;
    }

    /**
     * Update existing template data
     *
     * @param [type] $data
     * @param Event $event
     * @param PointTemplate $template
     * @param User $user
     */
    public function updateTemplate($data, Event $event, PointTemplate $template, User $user) {
        DB::beginTransaction();
        $this->checkUserId($user);

        $templateName =  $data['template_name'];

        $template->name = $templateName;
        $template->save();
        
        $this->deleteRemovedFieldsAndBlocks($data, $template);

        $this->createOrUpdateFields($data, $template);

        $this->assignTemplateToEvent($event, $template);
        
        DB::commit();
        
        return $template;
    }

    /**
     * Remove template fields, blocks, relation entry
     *
     * @param PointTemplate $template
     * @param User $user
     */
    public function removeEventPointTemplate(PointTemplate $template, User $user) {
        $fields = $template->fields()->get();

        foreach($fields as $field) {
            $blocks = $field->blocks()->get();

            foreach($blocks as $block) {
                $block->delete();
            }
            $field->delete();
        }

        EventPointTemplate::where([['point_template_id', '=', $template->id], ['user_id', '=', $user->id]])->delete();
    }

    /**
     * Return full template data with extras
     *
     * @param PointTemplate $template
     */
    public function getTemplateFull(PointTemplate $template) {
        $eventPointTemplate = PointTemplate::where('id', '=', $template->id)->with('fields.blocks')->first();
        return $eventPointTemplate;
    }

    /**
     * Remove template completely (fields, blocks, template, relation entry)
     *
     * @param PointTemplate $template
     * @param User $user
     */
    public function deleteTemplate(PointTemplate $template, User $user) {
        DB::beginTransaction();
        $this->checkUserId($user);

        try {
            $this->removeEventPointTemplate($template, $user);
            $template->delete();
        } catch (Exception $e) {
            DB::rollback();
            throw new ApiException("Can not remove template.");
        }
        DB::commit();
    }

    /**
     * Get the template for the specified event
     *
     * @param Event $event
     */
    public function getTemplateByEvent(Event $event) {
        $eventPointTemplate = EventPointTemplate::where('point_template_id', '=', $event->point_template_id)->get()->first();
        $template = new stdClass();
        
        if($eventPointTemplate && $eventPointTemplate->id) {
            $template = $eventPointTemplate->getTemplateAssoc();
        }

        $response = new stdClass();

        if($template && isset($template->id)) {
            $response = PointTemplate::where('id', '=', $template->id)->with('fields')->first();
        }

        return $response;
    }

    protected function createOrUpdateFields($data, PointTemplate $template) {
        $fields = $data['fields'];

        foreach($fields as $field) {
            if($field['id'] > 0) {
                $this->updateField($field, $template);
            } else {
                $this->addField($field, $template);
            }
        }
    }

    protected function addFields($data, PointTemplate $template) {
        $fields = $data['fields'];

        foreach($fields as $field) {
            $this->addField($field, $template);
        }
    }

    /**
     * Try to delete all fields the user removed
     *
     * @param [type] $data
     * @param PointTemplate $template
     * @return void
     */
    protected function deleteRemovedFieldsAndBlocks($data, PointTemplate $template) {
        $fieldsIn = $data['fields'];
        $fieldsSaved = $template->fields()->get();

        //delete remove list fields
        foreach($fieldsSaved as $field) {
            $isFieldRemoved = true;
            $fieldBlocksFromInField = [];

            //is field removed?
            foreach($fieldsIn as $fieldIn) {
                if($fieldIn['id'] == $field->id) {
                    $isFieldRemoved = false;
                    array_push($fieldBlocksFromInField, $fieldIn['blocks']);
                    continue;
                }
            }

            if($isFieldRemoved) {
                $this->deleteField($field);
            } else {
                $this->deleteRemovedBlocks($fieldBlocksFromInField, $field);
            }
        }
    }

    /**
     * Delete all blocks in Database that got removed by user
     *
     * @param [type] $blocksIn
     * @param [type] $field
     * @return void
     */
    protected function deleteRemovedBlocks($blocksIn, PointField $field) {
        $blocks = $field->blocks()->get();

        foreach($blocks as $block) {
            $isBlockRemoved = true;
            
            //is block removed?
            foreach($blocksIn[0] as $blockIn) {
                $parsedBlock = json_decode($blockIn);
                
                if(!isset($parsedBlock->id)) {
                    throw new ApiException("Can not remove the block \"$parsedBlock->name\". Block ID missing.");
                }

                if($parsedBlock->id == $block->id) {
                    $isBlockRemoved = false;
                    continue;
                }
            }

            if($isBlockRemoved) {
                $block->delete();
            }
        }
    }
    
    /**
     * Delete field
     *
     * @param Collection $fieldsSaved
     * @param PointField $field
     * @return void
     */
    private function deleteField(PointField $field) {
        $blocks = $field->blocks()->get();

        foreach($blocks as $block) {
            $block->delete();
        }

        if(!$this->canDeleteField($field)) {
            throw new ApiException("Canot adjust Template. The Field \"$field->name\" is being referenced in a match by a user. Remove all match data submitted by all users for this event or create an new template to resolve this issue.");
        }

        $field->delete();
    }

    /**
     * Can this field be deleted? | Is this field referenced in MatchDetails?
     *
     * @param PointField $field
     * @return boolean
     */
    private function canDeleteField(PointField $field) {
        return MatchDetail::where('point_field_id', '=', $field->id)->sum('id') == 0;
    }

    protected function addTemplate($templateName) {
        $template = new PointTemplate();
        $template->name = $templateName;
        $template->save();

        return $template;
    }

    protected function removeFields(PointTemplate $template) {
        $fields = $template->fields()->get();

        foreach($fields as $field) {
            $blocks = $field->blocks()->get();

            foreach($blocks as $block) {
                $block->delete();
            }
            $field->delete();
        }
    }

    protected function addEventPointTemplate(Event $event, PointTemplate $template, User $user) {
        $eventPointTemplate = new EventPointTemplate();
        $eventPointTemplate->point_template_id = $template->id;
        $eventPointTemplate->event_id = $event->id;
        $eventPointTemplate->user_id = $user->id;
        $eventPointTemplate->save();

        return $eventPointTemplate;
    }

    protected function updateEventPointTemplate(Event $event, PointTemplate $template, User $user) {
        $eventPointTemplate = new EventPointTemplate();
        $eventPointTemplate->point_template_id = $template->id;
        $eventPointTemplate->event_id = $event->id;
        $eventPointTemplate->user_id = $user->id;
        $eventPointTemplate->save();

        return $eventPointTemplate;
    }

    protected function addField($field, PointTemplate $template) {
        $fieldName = $field['name'];

        if(empty($fieldName)) {
            DB::rollback();
            throw new ApiException("Some field has no name. Please name every field before save.");
        }

        $pointField = new PointField();
        $pointField->name = $fieldName;
        $pointField->point_template_id = $template->id;
        $pointField->save();

        $blocks = $field['blocks'];
        $this->addBlocks($blocks, $pointField);

        return $pointField;
    }

    protected function updateField($field, PointTemplate $template) {
        $fieldName = $field['name'];
        $fieldId = intval($field['id']);
        
        if(empty($fieldName)) {
            DB::rollback();
            throw new ApiException("Some field has no name. Please name every field before save.");
        }

        $pointField = PointField::where('id', '=', $fieldId )->first();
        $pointField->name = $fieldName;
        $pointField->point_template_id = $template->id;
        $pointField->save();

        $blocks = $field['blocks'];
        $this->updateBlocks($blocks, $pointField);

        return $pointField;
    }

    
    protected function addBlocks($blocks, PointField $field) {
        foreach($blocks as $block) {
            $blockEncoded = json_decode($block);
            $this->addBlock($blockEncoded, $field);
        }
        
        return true;
    }
   
    protected function addBlock($block, PointField $field) {
        $pointBlock = new FieldBlock();
        $pointBlock->component_name = $block->component;
        $pointBlock->data = json_encode($block);
        $pointBlock->point_field_id = $field->id;
        $pointBlock->save();

        return $pointBlock;
    }

    protected function updateBlock($block, PointField $field) {
        $blockId = $block->id;

        $pointBlock = FieldBlock::where('id', '=', $blockId)->first();
        $pointBlock->component_name = $block->component;
        $pointBlock->data = json_encode($block);
        $pointBlock->point_field_id = $field->id;
        $pointBlock->save();

        return $pointBlock;
    }

    protected function createOrUpdateBlock($block, PointField $field) {
        if($block->id > 0) {
            $this->updateBlock($block, $field);
        } else {
            $this->addBlock($block, $field);
        }
    }

    protected function updateBlocks($blocks, PointField $field) {
        foreach($blocks as $block) {
            $blockEncoded = json_decode($block);
            $this->createOrUpdateBlock($blockEncoded, $field);
        }

        return true;
    }

    protected function checkUserId(User $user) {
        if($user->id <= 0) {
            throw new ApiException("Can not store template. User Id missing");
        }
    }

    protected function assignTemplateToEvent(Event $event, PointTemplate $template) {
        $event->point_template_id = $template->id;
        return $event->save();
    }
}
