<?php

namespace App\Repositories;

use App\Exceptions\ApiException;
use App\Models\DisqualifiedEventUsers;
use App\Models\Event;
use App\Models\User;
use App\Repositories\interfaces\BanRepositoryInterface;

class BanRepository implements BanRepositoryInterface {
    
    /**
     * add user to ban list of event
     *
     * @param User $user
     * @param Event $event
     * 
     * @return DisqualifiedEventUsers $ban
     */
    public function addUserToBanList(User $user, Event $event) : DisqualifiedEventUsers {
        $disq = new DisqualifiedEventUsers();
        if(!$this->isUserBanned($user, $event)) {
            $disq->user_id = $user->id;
            $disq->event_id = $event->id;
            $disq->save();
        }

        return $disq;
    }

    /**
     * Is user already banned from the event?
     * @param User $user
     * @param Event $event
     * @return boolean
     */
    public function isUserBanned(User $user, Event $event) : bool {
        return DisqualifiedEventUsers::where([['user_id', '=', $user->id], ['event_id', '=', $event->id]])->sum('id') > 0;
    }

    /**
     * Remove user from the events ban list
     *
     * @param User $user
     * @param Event $event
     * @return boolean
     */
    public function removeUserFromBanList(User $user, Event $event) : bool {
        return DisqualifiedEventUsers::where([['user_id', '=', $user->id], ['event_id', '=', $event->id]])->delete();
    }
}
