<?php

namespace App\Enums;

/**
 * Roles in app
 */
class RolesEnum {
    public const ADMIN = "admin";   //full access
    const USER = "user";            //default user
    const ORGANIZER = "organizer";  //can manage events, can review event submissions
    const REVIEWER = "reviewer";    //can review event submissions
}