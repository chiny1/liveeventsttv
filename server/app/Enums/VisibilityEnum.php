<?php

namespace App\Enums;

/**
 * possible Visibilities of events
 */
class VisibilityEnum {
    const PUBLIC = "public";
    const PRIVATE = "private";
}