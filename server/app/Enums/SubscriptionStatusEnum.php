<?php

namespace App\Enums;

/**
 * possible subscription of statuses
 */
class SubscriptionStatusEnum {
    const SUBSCRIBED = "subscribed";
    const UNSUBSCRIBED = "unsubscribed";
    const BLOCKED = "blocked";
}