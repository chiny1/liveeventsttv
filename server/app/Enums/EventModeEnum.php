<?php

namespace App\Enums;

/**
 * Event Modes
 */
class EventModeEnum {
    const SOLO = "Solo";
    const TEAM = "Team";
    const BOTH = "Solos and Teams";
}