<?php

namespace App\Enums;

/**
 * All permissions of app
 */
class PermissionsEnum {
    
    //USERS
    const USER_CREATE =     "user create";
    const USER_UPDATE =     "user update";
    const USER_DELETE =     "user delete";
    const USER_SHOW =       "user show";
    const DISQUALIFY_USER = "user disqualify";

    //TEAMS
    const TEAM_CREATE =     "team create";
    const TEAM_UPDATE =     "team update";
    const TEAM_DELETE =     "team delete";
    const TEAM_SHOW =       "team show";
    const DISQUALIFY_TEAM = "team disqualify";

    //EVENTS
    const EVENT_CREATE =    "event create";
    const EVENT_UPDATE =    "event update";
    const EVENT_DELETE =    "event delete";
    const EVENT_SHOW =      "event show";
    //reviewer permissions
    const EVENT_SUBMISSION_REVIEW =     "event submission show"; //see submissions
    const EVENT_SUBMISSION_ACCEPT =     "event submission accept"; //default behaviour?
    const EVENT_SUBMISSION_DECLINE =    "event submission decline"; //
}