<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Query "Filters" for Events Model
 */
trait EventQueryFilter {

    /**
     * Filter: return only private events
     * @param Builder $queryBuilder
     * @return Builder
     */
    private function addPrivateQueryFilter(Builder $queryBuilder) {
        return $queryBuilder->whereHas('visibility', function(Builder $query){
            $query->where('visibility', '=', "private");
        });
    }

    /**
     * Filter: return only public events
     * @param Builder $queryBuilder
     * @return Builder
     */
    private function addPublicQueryFilter(Builder $queryBuilder) {
        return $queryBuilder->whereHas('visibility', function(Builder $query){
            $query->where('visibility', '=', "public");
        });
    }
}